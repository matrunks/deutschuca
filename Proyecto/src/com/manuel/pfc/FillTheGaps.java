package com.manuel.pfc;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Clase que hereda de Activity y implementa el escuchador del tiempo. Esta clase se encarga de gestionar en su totalidad la actividad de rellenar huecos
 * @author Manuel Jesús Pérez Zurera
 */


public class FillTheGaps extends Activity implements TimerObserver{
	
	private boolean onTest;
	private boolean onFinish;
	private boolean saved;
	
	private int actualquestion;
	private int questioncount=0;
	private int maxquestion;
	private int limit;
	private int type;
	private int tries;
	private int secondslimit=180;
	
	private Mark marks;
	private MediaPlayer sound;
	private TimerClass timer;
	private TextView timer_view;
	
	private String color;
	
	private ArrayList<Integer> askedquestion;
	
	private Random random;

	private Date end;
	private Date begin;

	/**
	 * Constructor de la clase, se le debe pasar un entero en el intent con el tipo de actividad
	 * 1 Para lugares. 2 Para objetos. 3 para características.
	 */
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fill_the_gaps_layout);
		
		//pictures_sequence = new ArrayList<Integer>();
		
		askedquestion = new ArrayList <Integer>();
		random = new Random();
		
		color = "gris";
		
		if(savedInstanceState != null){	//si no es la primera vez que arranco esta activity
			instantiateElements(savedInstanceState);
		}else{
			
			//Cogemos el tipo, si es de tipo lugar o de armas
			Bundle bundle = getIntent().getExtras();
			type = bundle.getInt("type");
			
			tries=0;
			
			marks = new Mark();
			
			saved = false;
			
			int id = getResources().getIdentifier("fill_questionlimit", "integer", "com.manuel.pfc");
			
			limit = getResources().getInteger(id);	
			
			if(type == 1) //tipo lugares
			{
				id = getResources().getIdentifier("fill_maxplaces", "integer", "com.manuel.pfc");
				TextView text = (TextView) findViewById(R.id.questionText);
				
				text.setText(getString(getResources().getIdentifier("fillplaces", "string", "com.manuel.pfc")));
			}
			else if(type == 2) //tipo armas
			{
				
				id = getResources().getIdentifier("fill_maxweapons", "integer", "com.manuel.pfc");
				TextView text = (TextView) findViewById(R.id.questionText);
				
				text.setText(getString(getResources().getIdentifier("fillweapons", "string", "com.manuel.pfc")));
			}
			else if(type == 3) //tipo caras
			{
				
				id = getResources().getIdentifier("fill_maxfaces", "integer", "com.manuel.pfc");
				TextView text = (TextView) findViewById(R.id.questionText);
				
				text.setText(getString(getResources().getIdentifier("fillfaces", "string", "com.manuel.pfc")));
			}
			
			maxquestion = getResources().getInteger(id);
			
			do{
				actualquestion=obtenerInicial()+random.nextInt(maxquestion)+1;
				//System.out.println(actualquestion);
			}while(askedquestion.contains(actualquestion));
			
			questioncount++;
			askedquestion.add(actualquestion);
			
			onTest=false;
			onFinish=false;
		}
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón de sonido de la pregunta descriptiva de la actividad.
	 * La función reproducirá un sonido dependiendo del tipo de actividad en la que estemos
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onQuestionSoundClick(View button){
		if (type == 1)
		{
			int id = getResources().getIdentifier(getResources().getString(R.string.prefix)+"fills_places", "raw", "com.manuel.pfc");
			sound = MediaPlayer.create(getApplicationContext(), id);
			sound.start();
		}
		if (type == 2)
		{
			int id = getResources().getIdentifier(getResources().getString(R.string.prefix)+"fills_weapons", "raw", "com.manuel.pfc");
			sound = MediaPlayer.create(getApplicationContext(), id);
			sound.start();
		}
		if (type == 3)
		{
			int id = getResources().getIdentifier(getResources().getString(R.string.prefix)+"fills_faces", "raw", "com.manuel.pfc");
			sound = MediaPlayer.create(getApplicationContext(), id);
			sound.start();
		}
		
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar 'Aceptar' en la presentación de la actividad
	 * Se encarga de inicializar la partida y cargar lo necesario en pantalla (imágenes y texto) para comenzar la partida.
	 * Una vez ejecutada esta función se considera que estamos en la partida y el tiempo empieza a correr.
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onAcceptClick(View button){
		if(onTest){ //Si hemos terminado
			onFinish=true;
			//Cargamos el resultado
			loadResult();
		}else {
			onTest=true;
			//randomizePhotos();
			loadPhoto();
			
			//Activamos el begin time
			begin = new Date();
			begin.setTime(begin.getTime());
			
			//Activamos el end time
			end = new Date();
			end.setTime(end.getTime() + (secondslimit*1000));
			
			//Activamos el timer
			timer = new TimerClass(end);
			
			
			startTimerTask();
			reloadActivity();
		}
	}
	
	/**
	 * Método que como su nombre indica pone en marcha la tarea del reloj.
	 * El cronómetro comenzará a mandar avisos a través de los escuchadores inicializados.
	 */
	private void startTimerTask(){
		timer.start(this);
	}
	
	/**
	 * Método que esconde el teclado.
	 * Es útil para despejar la pantalla y mostrar más información
	 */
	private void hideSoftKeyBoard(){
		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		
		EditText et = (EditText) findViewById(R.id.secondpart);
			imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
	}
	
	/**
	 * Método que se ejecuta automáticamente una vez pulsado el botón send.
	 * Se encarga de analizar la frase escrita por el usuario y comprobar su validez con los recursos del sistema.
	 * Anotará un fallo por cada palabra incorrecta, y un acierto si la frase al completo está bien.
	 * Anotará los fallos cometidos para mostrarlos más tarde.
	 * Al tercer fallo, pondrá por pantalla una solución posible (de varias, siempre la primera).
	 * Reproducirá un sonido de acierto si hemos acertado.
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onSendClick(View button){
		//Cogemos el valor escrito por el usuario para el verbo
		EditText et1 = (EditText) findViewById(R.id.firstpart);
		
		//Solo funciona si hemos introducido algo coherente
		if(!et1.getText().toString().equals(" ") && !et1.getText().toString().equals("  ") && !et1.getText().toString().equals("   "))
		{
			//Con esta variable controlaremos si ha habido algún fallo en el ejercicio
			boolean error = false;
			
			//Cogemos la respuesta correcta para el verbo
			int id = getResources().getIdentifier("fill_firstpart"+actualquestion, "string", "com.manuel.pfc");
			String firstpart = getResources().getString(id);
			
			//Si en la respuesta correcta no contiene parte de lo escrito buscamos una alternativa
			if(!et1.getText().toString().replace("  ", "").equals(firstpart)){
				int i=1;
				boolean found = false;
				
				//Mientras que el id no sea null y no encontremos alternativa seguimos dando vueltas
				while((id = getResources().getIdentifier("fill_firstpart"+actualquestion+"."+i, "string", "com.manuel.pfc")) != 0 && found == false)
				{
					//Como no coincide vamos a buscar si existe alternativa
					firstpart = getResources().getString(id);
					
					//Si la alternativa coincide
					if(et1.getText().toString().replace("  ", "").equals(firstpart))
					{
						found = true;
						System.out.println("Alternativa encontrada");
					}
					
					i++;
				}
				
				//Si no encontramos alternativa se ha equivocado el alumno
				if(found == false)
				{
					System.out.println("Alternativa no encontrada");
					marks.scoreFail();
					error = true;
					
					//Si tenemos menos de dos intentos borramos
					if(tries <= 2){
						et1.setText("");
					}
				}
			}
			
			System.out.println("Primera respuesta "+et1.getText().toString().replace("  ", ""));
			
			System.out.println("Primera parte " + firstpart);
			
			EditText et2 = null;

			//Cogemos el valor escrito por el usuario para el verbo
			et2 = (EditText) findViewById(R.id.secondpart);
			
			//Cogemos la respuesta correcta para el lugar
			id = getResources().getIdentifier("fill_secondpart"+actualquestion, "string", "com.manuel.pfc");
			String secondpart = getResources().getString(id);
		
			//Si en la respuesta correcta no contiene parte de lo escrito la damos por invalida
			if(!et2.getText().toString().replace("  ", "").equals(secondpart)){
				int i=1;
				boolean found = false;
				
				//Mientras que el id no sea null y no encontremos alternativa seguimos dando vueltas
				while((id = getResources().getIdentifier("fill_secondpart"+actualquestion+"."+i, "string", "com.manuel.pfc")) != 0 && found == false)
				{
					//Como no coincide vamos a buscar si existe alternativa
					secondpart = getResources().getString(id);
					
					//Si la alternativa coincide
					if(et2.getText().toString().replace("  ", "").equals(secondpart))
					{
						found = true;
						System.out.println("Alternativa encontrada");
					}
					
					i++;
				}
				
				//Si no encontramos alternativa se ha equivocado el alumno
				if(found == false)
				{
					System.out.println("Alternativa no encontrada");
					marks.scoreFail();
					error = true;
					
					//Si tenemos menos de dos intentos borramos
					if(tries <= 2){
						et2.setText("");
					}
				}
			}
			
			System.out.println("Segunda respuesta "+et2.getText().toString().replace("  ", ""));
			
			System.out.println("Segunda parte " + secondpart);

			TextView text = (TextView) findViewById(R.id.correct_text);
			
			//Si ha habido error, anotaremos la frase y el número de frase
			if(error == true)
			{
				tries++;
				//Metemos la frase en pantalla
				String correct_sentence = noteSentence();
				
				//Si hemos fallado al menos 3 veces
				if(tries > 2)
				{
					text.setText(Html.fromHtml(correct_sentence),TextView.BufferType.SPANNABLE);	
				}
				else
				{
					//Ponemos la frase del try again
					id = getResources().getIdentifier("tryagain", "string", "com.manuel.pfc");
					String tryagain = getResources().getString(id);
					text.setText(tryagain);
				}
				
				text.setVisibility(View.VISIBLE);
			}
			else
			{
				//Reseteamos el valor de tries
				tries=0;
				
				//Aumentamos el número de la pregunta
				questioncount++;
				
				et1.setText("");
				
				if(et2 != null)
					et2.setText("");
				
				//Escondo el teclado
				hideSoftKeyBoard();
		
				marks.scoreCorrect();
				
				id = getResources().getIdentifier("success", "raw", "com.manuel.pfc");
				sound = MediaPlayer.create(getApplicationContext(), id);
				sound.start();

				text.setVisibility(View.GONE);
				checkEnd();
			}
		}
	}
	
	/*public void onHintClick(View button){
		marks.scoreHint();
	}*/
	
	/**
	 * Método que se encarga de anotar la frase correcta que tendría que haber escrito el usuario, para más tarde mostrarla.
	 * Por ejemplo si escribimos 'Ella ir al cine', este método anotará en memoria que nos equivocamos en la frase 'Ella va al cine' y la guardará con su formato de colores para resaltar en que nos equivocamos.
	 * @return Devuelve la frase en un objeto String que contiene el texto ya formateado con colores HTML.
	 */
	private String noteSentence(){
		
		String sentence = new String();
		int id;
		
		id = getResources().getIdentifier("fill_question"+actualquestion+".1", "string", "com.manuel.pfc");
		String question1 = getResources().getString(id);
		
		sentence = sentence + (question1);
		
		id = getResources().getIdentifier("fill_firstpart"+actualquestion, "string", "com.manuel.pfc");
		String first_part = getResources().getString(id);

		sentence = sentence + "<font COLOR=\"#008D13\"> " + (first_part) + " </font>";
		
		String question2 = new String();
		
		//Si es de tipo uno ademas tengo que registrar la segunda parte de la frase
		if(type == 1)
		{
			id = getResources().getIdentifier("fill_question"+actualquestion+".2", "string", "com.manuel.pfc");
			question2 = getResources().getString(id);
			
			sentence = sentence + (question2);
		}
		
		id = getResources().getIdentifier("fill_secondpart"+actualquestion, "string", "com.manuel.pfc");
		String secondpart = getResources().getString(id);
		
		sentence = sentence + "<font COLOR=\"#008D13\"> " + (secondpart) + " </font> /";
		int i=1;
		//Buscamos alguna alternativa a añadir
		while((id = getResources().getIdentifier("fill_firstpart"+actualquestion+"."+i, "string", "com.manuel.pfc")) != 0)
		{
			first_part = getResources().getString(id);
			secondpart = getResources().getString(getResources().getIdentifier("fill_secondpart"+actualquestion+"."+i, "string", "com.manuel.pfc"));
			
			sentence = sentence + "\n <p>" + question1 + "<font COLOR=\"#008D13\"> " + first_part + " </font>" + question2 + "<font COLOR=\"#008D13\"> " + secondpart + " </font> / </p>";
			i++;
		}

		marks.noteSentence(sentence, questioncount);
		
		return sentence;
	}
	
	/**
	 * Método que se encarga de comprobar si hemos llegado al final.
	 * Se ejecuta cada vez que pulsamos 'Send' es decir cada vez que respondemos a una pregunta. Si hemos superado el límite de preguntas cargará la función loadResult y dará por finalizada la actividad.
	 */
	private void checkEnd(){
		if(questioncount>limit){
			onFinish=true;
			onTest=false;
			loadResult();
		}
		else{
			
			//Seleccionamos una nueva pregunta que no haya sido preguntada
			do{
				actualquestion=obtenerInicial()+random.nextInt(maxquestion)+1;
				System.out.println(actualquestion);
			}while(askedquestion.contains(actualquestion));
			
			askedquestion.add(actualquestion);
			
			//Las cargamos en el orden anterior
			loadPhoto();
			
			reloadActivity();
		}
		
	}
	
	/**
	 * Método que se encarga de cargar la foto contenida en la pregunta actual.
	 */
	private void loadPhoto(){
		ImageView image = (ImageView) findViewById(R.id.picture_question);
		
		int id=0;
		
		if(type == 1){
			id = getResources().getIdentifier("fills_image"+actualquestion, "drawable", "com.manuel.pfc");
		}
		
		if(type == 2){
			id = getResources().getIdentifier("search_image"+actualquestion, "drawable", "com.manuel.pfc");
		}
		
		if(type == 3){
			id = getResources().getIdentifier("search_image"+actualquestion, "drawable", "com.manuel.pfc");
		}
				
		image.setImageResource(id);
	}
	
	/**
	 * Método que sirve para recargar la actividad, pudiendo haber sido después de un giro de pantalla o simplemente al cambiar de pregunta
	 */
	private void reloadActivity(){
		//Ponemos el texto del tiempo
		timer_view = (TextView) findViewById(R.id.secondsTimerView);
		timer_view.setVisibility(View.VISIBLE);
				
		LinearLayout ll = (LinearLayout) findViewById(R.id.head_layout);
		ll.setVisibility(View.VISIBLE);
		
		ll = (LinearLayout) findViewById(R.id.picture_layout);
		ll.setVisibility(View.VISIBLE);
		
		Button b=(Button)findViewById(R.id.SendButton);
		b.setVisibility(View.VISIBLE);
		
		//b=(Button)findViewById(R.id.HintButton);
		//b.setVisibility(View.VISIBLE);
		
		b=(Button)findViewById(R.id.soundQuestionButton);
		b.setVisibility(View.VISIBLE);

		if(type == 2 || type == 3)
		{
			TextView text = (TextView) findViewById(R.id.text2);
			text.setVisibility(View.GONE);
		}
		
		System.out.println("Type "+type);
		
		//Si estamos en modo 2 no nos hace falta dos cajas de texto
		
		TextView text = (TextView) findViewById(R.id.questionText);
		if(type == 1)
		{	
			text.setText(getString(getResources().getIdentifier("fillplaces", "string", "com.manuel.pfc")));
		}
		
		if(type == 2)
		{	
			text.setText(getString(getResources().getIdentifier("fillweapons", "string", "com.manuel.pfc")));
		}
		
		//Si es tipo tres nos hace falta dejar un texto y dos cajas
		if(type == 3)
		{
			text.setText(getString(getResources().getIdentifier("fillfaces", "string", "com.manuel.pfc")));
		}
		
		text.setVisibility(View.VISIBLE);

		loadPhoto();
		
		ll = (LinearLayout) findViewById(R.id.fill_layout);
		ll.setVisibility(View.VISIBLE);
		
		//Comienza la prueba y escondemos el accept button
		b=(Button)findViewById(R.id.AcceptButton);
		b.setVisibility(View.GONE);
						
		//Escondemos la imagen del profesor
		ImageView image=(ImageView)findViewById(R.id.image_professor);
		image.setVisibility(View.GONE);
				
		text = (TextView) findViewById(R.id.fillthegapsText);
		text.setVisibility(View.GONE);
		
		//Cargamos la frase personalizada por pregunta
		loadPhrase();
		
		text = (TextView) findViewById(R.id.fillthegapsText2);
		text.setVisibility(View.GONE);	
	}
	
	/**
	 * Método que se encarga de cargar la frase actual en la pantalla para que el jugador pueda entender el contexto de la foto.
	 */
	private void loadPhrase(){
		int id = getResources().getIdentifier("fill_question"+actualquestion+".1", "string", "com.manuel.pfc");
		
		TextView text = (TextView) findViewById(R.id.text1);
		text.setText(getResources().getString(id));
		
		if(type == 1){
			id = getResources().getIdentifier("fill_question"+actualquestion+".2", "string", "com.manuel.pfc");
			
			text = (TextView) findViewById(R.id.text2);
			text.setText(getResources().getString(id));
		}
		
	}
	
	
	
	/**
	 * Método que se encarga de mostrar correctamente la puntuación y las frases correctas en las que habíamos fallado.
	 * También para el cronómetro y comprueba mediante la clase DatabaseHelper si es necesario guardar la puntuación o no.
	 */
	private void loadResult(){
		//Paramos los sonidos para que no carguen más fotos
		if(sound != null)
		{
			sound.stop();
			sound.release();
		}
		
		//Cerramos el reloj
		timer.stop();
		
		//Quitamos el texto del tiempo
		timer_view = (TextView) findViewById(R.id.secondsTimerView);
		timer_view.setVisibility(View.GONE);
		
		LinearLayout ll = (LinearLayout) findViewById(R.id.picture_layout);
		ll.setVisibility(View.GONE);
		
		ll = (LinearLayout) findViewById(R.id.head_layout);
		ll.setVisibility(View.GONE);
		
		ll = (LinearLayout) findViewById(R.id.fill_layout);
		ll.setVisibility(View.GONE);
		
		Button b=(Button)findViewById(R.id.SendButton);
		b.setVisibility(View.GONE);
		
		//b=(Button)findViewById(R.id.HintButton);
		//b.setVisibility(View.GONE);
		
		b=(Button)findViewById(R.id.soundQuestionButton);
		b.setVisibility(View.GONE);
		
		TextView text = (TextView) findViewById(R.id.fillthegapsText2);
		text.setVisibility(View.GONE);	
		
		text = (TextView) findViewById(R.id.correct_text);
		text.setVisibility(View.GONE);	
		
		ll = (LinearLayout) findViewById(R.id.result_layout);
		ll.setVisibility(View.VISIBLE);
		
		ImageView image=(ImageView)findViewById(R.id.image_professor);
		image.setVisibility(View.VISIBLE);
		
		//Cogemos el tiempo actual
		Date now = new Date();
		now.setTime(now.getTime());
		
		String time_taken = timer.differenceToString(begin, now, secondslimit);
		
		//Mostramos los aciertos
		text =(TextView)findViewById(R.id.markCorrectText);
		text.setText(marks.printCorrects()+ "\n" +getResources().getString(R.string.time_taken1)+" "+time_taken+" "+getResources().getString(R.string.time_taken2));
		text.setVisibility(View.VISIBLE);
		
		//Si tenemos algún fallo lo mostramos	
		if(marks.getFails()!=0){
			text =(TextView)findViewById(R.id.markFailText);
			text.setText(marks.printFails());
			text.setVisibility(View.VISIBLE);
			
			text =(TextView)findViewById(R.id.markSentencesText);
			text.setText(Html.fromHtml(marks.printCorrectSentences()),TextView.BufferType.SPANNABLE);
			text.setVisibility(View.VISIBLE);
		}

		//Si hemos usado alguna pista lo decimos	
		/*if(marks.getHints()!=0){
			text =(TextView)findViewById(R.id.markHintText);
			text.setText(marks.printHints());
			text.setVisibility(View.VISIBLE);
		}*/
		
		//Mostramos el botón retry
		b=(Button)findViewById(R.id.RetryButton);
		b.setVisibility(View.VISIBLE);
			
		text=(TextView)findViewById(R.id.fillthegapsText);
		text.setText(R.string.fill_end);
		text.setVisibility(View.VISIBLE);
		
		b=(Button)findViewById(R.id.ExitButton);
		b.setVisibility(View.VISIBLE);
		
		b=(Button)findViewById(R.id.AcceptButton);
		b.setVisibility(View.GONE);
		
		//Comprobamos como hemos puntuado con anterioridad
		if(saved == false)
		{
			if(DatabaseHelper.checkMarkInDB(3,type,this,marks.getCorrects(),marks.getFails()))
			{
				//Guardamos en la base de datos los resultados
				DatabaseHelper.saveToDB(3,type,this,marks.getCorrects(),marks.getFails(),time_taken);
				
				saved = true;
				
			}
		}
		
		
		//Si aún quedan por aprobar
		if(DatabaseHelper.checkRemaining(3, type, this) > 0)
		{
			showToast("Mach die Übung noch "+ DatabaseHelper.checkRemaining(3, type, this)+ " mal!");
		}
		//Si hemos aprobado pero ya tenemos todas aprobadas
		else if(DatabaseHelper.checkRemaining(3, type, this) == 0)
		{
			int activity;
			int type;
			
			if(DatabaseHelper.loadSaveGame(this) == 10)
			{
				showToast("Super! Weiter so! Mach Chat");
			}
			else
			{
				//Lo convierto a la actividad
				activity = (int) Math.ceil(((double) (DatabaseHelper.loadSaveGame(this))/3));
				type = (DatabaseHelper.loadSaveGame(this)%3);
				if (type == 0)
					type = 3;
				
				showToast("Super! Weiter so! Mach jetzt Übung "+  activity  +"."+ type +"!");
			}
		}
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón repetir
	 * Se encarga de resetear el cronómetro y de empezar una nueva actividad
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onRetryClick(View button){
		if(timer != null)
			timer.stop();
		
		Intent intent = new Intent();
		intent.setClass(this, FillTheGaps.class);
		intent.putExtra("type", type);
		
		if(sound != null){
			sound.release();
		}
		
		startActivity(intent);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón salir
	 * Se encarga de resetear el cronómetro y de volver al menú
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onExitClick(View button){
		if(timer != null)
			timer.stop();
		
		Intent intent = new Intent();
		intent.setClass(this, MainActivity.class);
		
		if(sound != null){
			sound.release();
		}
		
		startActivity(intent);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón atrás de Android
	 * Se encarga de resetear el cronómetro y de volver al menú
	 */
	public void onBackPressed(){
		if(timer !=null)
		{
			timer.stop();
		}
		
		System.out.println("Saliendo");
		
		Intent intent = new Intent();
		intent.setClass(this, MainActivity.class);
		
		if(sound != null){
			sound.release();
		}
		
		startActivity(intent);
	}
	
	/**
	 * Método que se ejecuta automáticamente al poner a la activity en segundo plano
	 * Se encarga de calcular la diferencia de tiempo ocurrida mientras estaba, ya que si dejamos la aplicación en segundo plano no sigue contando el tiempo.
	 */
	public void onRestart(){
		super.onRestart();
		
		//Calculamos el tiempo que ha pasado mientras la app estaba parada
		/*Date now = new Date();
		now.setTime(now.getTime());
		
		//Hacemos la diferencia entre las dos fechas
		int diff = (int) ((end.getTime() - now.getTime())/1000);
		*/
		
		
		if(timer != null && end != null)
		{
			int diff = TimerClass.getRemainingTime(end);
			
			System.out.println("Tiempo restante: "+diff);
			
			System.out.println("On restart ha activado un secundero "+diff);
			timer.stop();
			timer = new TimerClass(end);
			startTimerTask();
		}
	}
	
	/**
	 * Método privado que servirá para poder calcular que numeración tienen los recursos de la actividad en concreto
	 * @return Devuelve el número por el que empiezan los recursos de la actividad
	 */
	private int obtenerInicial(){
		if(type == 2)
			return 100;
		else if(type == 3)
			return 200;
		else
			return 0;
		
	}
	
	/**
	 * Método para cargar un estado anterior después de haberse producido un giro de la aplicación
	 * @param inState Objeto que guarda el estado de las variables antes de que se produjese el giro
	 */
	private void instantiateElements(Bundle inState){ //y aqui los recupero
		onTest=inState.getBoolean("onTest");
		onFinish=inState.getBoolean("onFinish");
		saved=inState.getBoolean("saved");
		askedquestion=inState.getIntegerArrayList("askedquestion");
		marks=inState.getParcelable("marks");
		questioncount=inState.getInt("questioncount");
		maxquestion=inState.getInt("maxquestion");
		actualquestion=inState.getInt("actualquestion");
		type=inState.getInt("type");
		limit=inState.getInt("limit");
		tries=inState.getInt("tries");
		end=(Date) inState.getSerializable("end");
		begin=(Date) inState.getSerializable("begin");

		if(end != null)
			timer = new TimerClass(end);
		
		if(onFinish){
			loadResult();
		}
		else if(onTest){
			reloadActivity();
			startTimerTask();
		}
	
	}
	/**
	 * Método que sirve para guardar el estado de la activity cuando se produce un giro de la pantalla
	 * @param outState Objeto de tipo Bundle que guardará el estado de las variables de la actividad.
	 */
	@Override //Cuando gire o se pare la activity guardo los valores
	protected void onSaveInstanceState(Bundle outState) {
	    // Save the values you need from your textview into "outState"-object
	    super.onSaveInstanceState(outState);

	    //guardamos las variables
	    outState.putBoolean("onTest", onTest);
	    outState.putBoolean("onFinish", onFinish);
	    outState.putBoolean("saved", saved);
	    outState.putInt("maxquestion", maxquestion);
	    outState.putInt("actualquestion", actualquestion);
	    outState.putInt("questioncount", questioncount);
	    outState.putInt("type", type);
	    outState.putInt("limit", limit);
	    outState.putInt("tries", tries);
	    outState.putIntegerArrayList("askedquestion", askedquestion);
	    outState.putParcelable("marks", marks);
	    outState.putSerializable("end", end);
	    outState.putSerializable("begin", begin);

	    //por último paro el reloj
	    if(timer != null)
	    	timer.stop();
	}
	
	/**
	 * Muestra un mensaje en pantalla con las notificaciones de Android
	 * @param toast Mensaje en formato String 
	 */
	public void showToast(final String toast)
	{
	    runOnUiThread(new Runnable() {
	        public void run()
	        {
	        	//Como no puedo decir que dure el doble, hago que salga dos veces
	        	for(int i = 0 ; i<2 ; i++)
	        		Toast.makeText(FillTheGaps.this, toast, Toast.LENGTH_LONG).show();
	        }
	    });
	}

	/**
	 * Método que se ejecuta cuando el cronómetro llega a su fin, se encarga de cargar el final de la partida indistintamente de lo que estemos haciendo
	 */
	public void notifyTimer() {
		runOnUiThread(new Runnable(){
			public void run()  
	     	{ 
				//hidePhoto();
				//Nos ha avisado el reloj de un evento, en mi caso será cerrar la app
				loadResult();
	     	} 
			
		});
    }
	
	/**
	 * Método que se encarga de avisar cada segundo de que hay que cambiar el reloj que aparece en la parte superior de la aplicación. 
	 * Cuando queden menos de diez segundos cambiará de color entre gris y rojo.
	 */
	public void notifySeconds() {
		runOnUiThread(new Runnable(){
			public void run()  
	     	{ 
				//Si hay menos de 10 segundos vamos cambiando el color del texto
				if(timer.getRemainingTime() < 10)
				{
					if(color.equals("gris")){
						timer_view.setBackgroundColor(Color.parseColor("#FF0000"));
						color="rojo";
					}
						
					else
					{
						timer_view.setBackgroundColor(Color.parseColor("#E6E6E6"));
						color="gris";
					}
							
				}
				
				
				//Actualizamos el text view
				timer_view.setText(getResources().getString(R.string.remaining_time)+" "+timer.getRemainingTimeToString());
				
				//Nos ha avisado el reloj de que ha pasado un segundo
				System.out.println(timer.getSecondsToString());
	     	} 
			
		});
    }
	
}
