package com.manuel.pfc;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.format.Time;

/**
 * Clase que sirve como interfaz para las bases de datos de la aplicación
 * @author Manuel Jesús Pérez Zurera
 */


public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "library.db";
	
	public static final String USERNAME = "name";
	public static final String ACTIVITY = "activity";
	public static final String CORRECTS = "corrects";
	public static final String FAILS = "fails";
	public static final String DATE = "date";
	public static final String TIME = "time";


	public DatabaseHelper (Context context){
		//el uno es la versión
		super(context, DATABASE_NAME, null,1);
	}
	
	/**
	 * Método que se ejecuta al crear un objeto DatabaseHelper
	 * @param db Objeto SQLiteDatabase que contiene la información de la base de datos que vamos a inicializar
	 */
	public void onCreate (SQLiteDatabase db){
		//borro mi db anterior
		db.execSQL( "DROP TABLE IF EXISTS users" );
		
		//Crea una tabla llamada usuarios con un id como clave primaria autoincremental y un nombre con formato texto
		db.execSQL( "CREATE TABLE users (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT);");
		
		//Crea una tabla llamada puntuaciones con un id, un numero de actividad, un entero de aciertos, un entero de fallos y una fecha que por defecto es la actual con horas y minutos
		db.execSQL( "CREATE TABLE marks (_id INTEGER PRIMARY KEY AUTOINCREMENT, activity INTEGER, corrects INTEGER, fails INTEGER, date DATETIME, time TEXT );");
	}
	
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		//android.util.Log.v("Constants", "Upgrading database,  which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS users");
		db.execSQL("DROP TABLE IF EXISTS marks");
		
		onCreate(db);
	}
	
	 /**
     * Método estático que nos proporciona el número de veces que nos quedan para aprobar una actividad
     * @param activity Numero de la actividad de la que queremos comprobar lo que nos falta por aprobar
     * @param type Tipo de la actividad de la que queremos comprobar lo que nos falta por aprobar
     * @param context Objeto Context necesario para usar el método estático
     * @return Devuelve el número de veces restantes que nos queda por aprobar una actividad
     */
	public static int checkRemaining(int activity, int type, Context context)
	{
		DatabaseHelper databaseHelper = new DatabaseHelper(context);
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		
		//Seleccionamos las columnas id y nombre
		String colum[]={"_id", "activity", "corrects", "fails", "date"};
		
		int limit = 0;
		int limitpoints = 0;
		
		//Depende de la activity el limite de notas es 3 o 2
		if(activity == 3)
		{
			limit = 2;
			limitpoints = 6;
		}
		else if(activity == 2 || activity == 1)
		{
			limit = 3;
			limitpoints = 7;
		}
		
		//La actividad realmente será una operación entre el tipo de objetos de la actividad y el activity
		activity = activity + ((type-1) * 3);
		
		//El where será activity = numero de la actividad
		String where = DatabaseHelper.ACTIVITY + "=" + activity;
		Cursor c = db.query("marks", colum, where ,null,null,null,null);
		
		int corrects_index, fails_index;
		corrects_index=c.getColumnIndex("corrects");
		fails_index=c.getColumnIndex("fails");
		
		//Recorremos la BD
		c.moveToLast();
		
		//Recogemos los valores de la base de Datos
		int correctsDB=c.getInt(corrects_index);
		int failsDB=c.getInt(fails_index);

		//Comprobamos si la última actividad está 'suspensa'
		if(correctsDB-failsDB < limitpoints)
		{
			//Ceramos la conexión ya no nos hace falta
			db.close();
			
			return (limit - c.getCount() + 1);
		}
		else
		{
			db.close();
			
			return (limit - c.getCount());
		}
		
	}
	
	 /**
     * Método que nos proporciona la partida guardada que tenemos
     * @param context Contexto de la aplicación necesario para consultar la base de datos
     * @return Devuelve el número de la última actividad superada
     */
	public static int loadSaveGame(Context context)
	{
		DatabaseHelper databaseHelper = new DatabaseHelper(context);
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		
		//Seleccionamos las columnas id y nombre
		String colum[]={"_id", "activity", "corrects", "fails"};
		
		int last = 1;
		
		for(int i=1; i<10; i++)
		{
			//Hacemos un query a la tabla Usuarios seleccionando id y nombre
			
			int activity = i;
			
			//El where será activity = numero de la actividad
			String where = DatabaseHelper.ACTIVITY + "=" + activity;
			Cursor c = db.query("marks", colum, where ,null,null,null,null);
			
			int corrects_index, fails_index;
			corrects_index=c.getColumnIndex("corrects");
			fails_index=c.getColumnIndex("fails");
			
			//Recorremos la BD
			c.moveToLast();
			
			System.out.println("Comprobando "+i +c.getCount());
		
			//Si la actividad es múltiplo de 3, solo exigiremos 2 aprobados
			if((i%3) == 0)
			{
				//Si hay registros en la BD devuelv true y escribimos el primer registro
				if(c.getCount() >= 2){ 
					System.out.println(c.getCount()+ " Registros en la actividad "+i);
					
					int corrects=c.getInt(corrects_index);
					int fails=c.getInt(fails_index);
					
					System.out.println("Comprobando "+i+" última nota" + (corrects-fails));
					
					//Si tiene más nota de la necesaria activamos la siguiente partida
					if((corrects-fails) >= 6){
						last = i+1;
					}
				}
				
			}
			else
			{
				//Si hay al menos 3 actividades hechas, significa que hay dos aprobadas, vamos a comprobar la última
				if(c.getCount() >= 3){ 
					System.out.println(c.getCount()+" Registros en la actividad "+i);
					
					int corrects=c.getInt(corrects_index);
					int fails=c.getInt(fails_index);
					
					System.out.println("Comprobando "+i+" última nota" + (corrects-fails));
					
					//Si tiene más nota de la necesaria activamos la siguiente partida
					if((corrects-fails) >= 7){
						last = i+1;
					}
				}
			}
		}
		
		db.close();
		
		return last;
	}
	
	 /**
     * Método que comprueba dado una actividad, un tipo, y su balance de correctas y falladas, si la hemos superado o si la última estaba superada
     * @param activity Es la activity de la que queremos consultar la información
     * @param type Tipo de la actividad de la que queremos consultar la información
     * @param context Contexto de la aplicación necesario para consultar la base de datos
     * @param corrects Número de preguntas correctamente respondidas en la actividad actual
     * @param fails Número de fallos cometidos en la actividad actual
     * @return Devuelve true si: la última actividad estaba suspensa y ha sido superada, si no había actividad anterior o si la anterior estaba aprobada y aún queda por aprobar. En caso contrario devuelve false
     */
	public static boolean checkMarkInDB(int activity, int type, Context context, int corrects,int fails){
		//Comprobamos que nuestra marca sea mejor que la almacenada
		
		DatabaseHelper databaseHelper = new DatabaseHelper(context);
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		
		//Seleccionamos las columnas id y nombre
		String colum[]={"_id", "activity", "corrects", "fails", "date"};
		
		int limit = 0;
		int limitpoints = 0;
		
		//Depende de la activity el limite de notas es 3 o 2
		if(activity == 3)
		{
			limit = 2;
			limitpoints = 6;
		}
		else if(activity == 2 || activity == 1)
		{
			limit = 3;
			limitpoints = 7;
		}
		
		//La actividad realmente será una operación entre el tipo de objetos de la actividad y el activity
		activity = activity + ((type-1) * 3);
		
		//El where será activity = numero de la actividad
		String where = DatabaseHelper.ACTIVITY + "=" + activity;
		Cursor c = db.query("marks", colum, where ,null,null,null,null);
		
		int corrects_index, fails_index, id_index, date_index;
		corrects_index=c.getColumnIndex("corrects");
		fails_index=c.getColumnIndex("fails");
		id_index=c.getColumnIndex("_id");
		date_index=c.getColumnIndex("date");
		
		//Recorremos la BD
		c.moveToLast();
		
		//Si no hay registros en la BD devuelvo true y escribimos el primer registro
		if(c.getCount()<1){ 
			db.close();
			return true;
		}
		//Si ya tenemos el máximo de registros, solo sobreescribimos si estaba suspensa y ahora hemos aprobado
		else if(c.getCount()>=limit)
		{
			//Recogemos los valores de la base de Datos
			int correctsDB=c.getInt(corrects_index);
			int failsDB=c.getInt(fails_index);
			int idDB=c.getInt(id_index);
			String date = c.getString(date_index);
			
	
			//Comprobamos si la última actividad está 'suspensa' y si la hemos superado
			if(correctsDB-failsDB < limitpoints && (corrects-fails > (correctsDB-failsDB)))
			{
				System.out.println("Borrando: "+" "+date+correctsDB+" "+failsDB+" "+idDB);
				
				//Entonces borramos la última y damos el visto bueno para una nueva
				db.execSQL("DELETE from marks where _id ="+idDB);
				
				//Ceramos la conexión ya no nos hace falta
				db.close();
				
				return true;
			}
			else
			{
				System.out.println("Límite superado y (ultima aprobada) o (no superada)");
				db.close();
				return false;
			}
		}
		//Solo podemos tener un numero de aprobados
		else if(c.getCount()<limit)
		{
			//Recogemos los valores de la base de Datos
			int correctsDB=c.getInt(corrects_index);
			int failsDB=c.getInt(fails_index);
			int idDB=c.getInt(id_index);
			String date = c.getString(date_index);
			
	
			//Comprobamos si la última actividad está 'suspensa' y si la hemos superado
			if(correctsDB-failsDB < limitpoints && (corrects-fails > (correctsDB-failsDB)))
			{
				System.out.println("Borrando: "+" "+date+correctsDB+" "+failsDB+" "+idDB);
				
				//Entonces borramos la última y damos el visto bueno para una nueva
				db.execSQL("DELETE from marks where _id ="+idDB);
				
				//Ceramos la conexión ya no nos hace falta
				db.close();
				
				return true;
			}
			//Comprobamos que la última está aprobada
			if(correctsDB-failsDB >= limitpoints)
			{
				System.out.println("La última actividad está aprobada, permiso para guardar");
				db.close();
				return true;
			}
			else
			{
				System.out.println("La última actividad no está aprobada y no ha sido superada denegado permiso");
				db.close();
				return false;
			}
		}
		
		System.out.println("Denegado permiso para escribir limite: "+limit +" actividades hechas"+c.getCount());
		db.close();
		
		return false;
	}
	
	
	/**
	 * Método estático para poder guardar para una actividad y un tipo un conjunto de fallos y aciertos
	 * @param activity Es la activity de la que queremos guardar la información
     * @param type Tipo de la actividad de la que queremos guardar la información
     * @param context Contexto de la aplicación necesario para consultar la base de datos
     * @param corrects Número de preguntas correctamente respondidas en la actividad actual
     * @param fails Número de fallos cometidos en la actividad actual
     * @param time Tiempo en formato MM:SS que nos ha llevado completar la actividad
	 */
	public static void saveToDB(int activity, int type, Context context, int corrects, int fails, String time){
		DatabaseHelper databaseHelper = new DatabaseHelper (context);
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		
		//El valor de la actividad corresponderá al tipo de actividad, 1,4,7 
		activity = activity + ((type-1) * 3);

		//String where = DatabaseHelper.ACTIVITY + "=" + activity;
		
		//Borramos la puntuación anterior para dejar unicamente la mejor
		//db.delete("marks", where, null);
		
		//Preparamos el tiempo para que pueda ser insertado en una database
		time = time.replace(":", "m ");
		time +="s";
		
		//Unica manera de poner bien la hora local!!!!!!!
		db.execSQL("insert into marks (activity,corrects,fails,date,time) values("+activity+","+corrects+","+fails+", datetime('now','localtime'),\""+time.toString()+"\")");
		
		System.out.println("Guardando en la tabla marks "+ "Actividad: "+ activity + " Puntuacion " + corrects + " - " + fails+ " "+Time.HOUR+":"+Time.MINUTE);
		
		//Ciero la conexión con la BD
		db.close();	
	}
}
