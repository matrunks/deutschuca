package com.manuel.pfc;

import android.app.Application;
import android.content.res.Resources;
import android.text.format.Time;


/**
 * Clase que extiende de Application y se encarga de guardar los objectos comunes a toda la aplicación.
 * Un objeto de ésta clase estará activo a lo largo del período de vida de la aplicación, proporcionando ciertas variables necesarias.
 * @author Manuel Jesús Pérez Zurera
 *
 */

//si queremos pasar objetos entre activities

public class PFC extends Application {
	
	private ChatHelper chathelper;
	private String multi_chat_log;
	private Time time;
	private Assignment assignment;
	private BlocData bloc;
	
	public PFC(){
		super();
		chathelper = new ChatHelper();
		time = new Time();
		multi_chat_log = new String();
		bloc = new BlocData();
	}
	
	/**
	 * 
	 * @return Devuelve el ChatHelper que contiene la aplicación
	 */
	public ChatHelper getChatHelper(){
		return chathelper;
	}
	
	/**
	 * 
	 * @return Devuelve la cadena de texto de la conversación grupal
	 */
	public String getMultiChatLog(){
		return multi_chat_log;
	}
	
	/**
	 * Método encargado de escribir en el log de la conversación
	 * @param text_msg Texto que pasará a ser el log de la conversación
	 */
	public void putMultiChatLog(String text_msg){
		multi_chat_log=text_msg;
	}
	
	/**
	 * Método encargado de poner el log de la conversación vacío
	 * 
	 */
	public void resetMultiChatLog(){
		multi_chat_log=" ";
	}
	
	/**
	 * Método encargado de proporcionar la hora
	 * @return Devuelve la hora en formato cadena [HH:MM:SS]
	 */
	public String getTime(){
		time.setToNow();
		return "["+String.format("%02d", time.hour)+":"+String.format("%02d", time.minute)+":"+String.format("%02d", time.second)+"]";
	}
	
	/**
	 * Método encargado de inicializar el Assignment que contendrá la tarea del chat
	 * @param res Recurso necesario para inicializar la Assignment
	 */
	public void initializeAssignment(Resources res){
		assignment= new Assignment(res);
	}
	
	/**
	 * @return Devuelve el Assignment con la tarea que está ejecutandose en el momento
	 */
	public Assignment getAssignment(){
		return assignment;
	}
	
	/**
	 * 
	 * @return Devuelve el blog de notas del comisario actual
	 */
	public BlocData getBloc(){
		return bloc;
	}

}