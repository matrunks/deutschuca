package com.manuel.pfc;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;


/**
 * Clase que extiende de Fragment y que gestiona el fragmento de la conversación del chat
 * @author Manuel Jesús Pérez Zurera 
 *
 */


public class ChatConversationFragment extends Fragment {
	private EditText message_text;
	private TextView text;
	private TextView timer_view;
	private Button btn_bloc;
	private ScrollView sv;
	private onChatConversationListener mCallback;
	private View view;
		
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
	   final View view = inflater.inflate(R.layout.chat_conversation_fragment,
	        container, false);
	    
	   setRetainInstance(true);
	   
	   this.view = view;
	   
	   text = (TextView) view.findViewById(R.id.text);
	   message_text=(EditText) view.findViewById(R.id.message_text);

	   btn_bloc = (Button) view.findViewById(R.id.btn_bloc);
	   //texts.add((TextView) view.findViewById(R.id.text));
	   sv = (ScrollView) view.findViewById(R.id.scrollView);
	   
	   timer_view = (TextView) view.findViewById(R.id.secondsTimerView);
	   

		//Preparamos el escuchador de eventos de los textos para que cuando se haga focus
		//sobre él desaparezca el mensaje de 'Escribe aqui'
		
		/*message_text.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
			    if(hasFocus){
			        //Toast.makeText(view.getContext().getApplicationContext(), "got the focus", Toast.LENGTH_LONG).show();
			    	message_text.setText("");
			    	showSoftKeyBoard();
			    }/*else {
			        //Toast.makeText(view.getContext().getApplicationContext(), "lost the focus", Toast.LENGTH_LONG).show();
			    	message_text.setText("Notier bitte hier!");
			    }*/
			  /* }
			});*/
	  
	   
	   //damos el mensaje de que hemos terminado
	   mCallback.onChatConversationFinish(true);
	   return view;
	 }
	
	// Container Activity must implement this interface
    public interface onChatConversationListener {
        public void onChatConversationFinish(boolean finish);
    }
    
    /**
     * Metodo para mostrar el teclado de manera automática
     */
    private void showSoftKeyBoard(){
		InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		
		System.out.println("Mostrando teclado");
		
		imm.showSoftInput(message_text, 0);
	}
    
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onChatConversationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    
    /**
     * Método que devuelve la visibilidad al botón del Blog del comisario
     */
    public void setVisibileBlocButton(){
		btn_bloc.setVisibility(View.VISIBLE);
    }
    
    /**
     * Método que devuelve la visibilidad al cronómetro
     */
    public void setVisibileTimerView(){
    	timer_view.setVisibility(View.VISIBLE);
    }
    
    /**
     * Método que fija el texto al texto del cronómetro
     * @param text Texto en formato String que queremos fijar en el texto del crónometro
     */
    public void setTimerText(String text){
    	timer_view.setText(text);
    }
    
    /**
     * Método que fija el color del fondo del texto del cronómetro
     * @param color Código del color en formato String que queremos fijar al texto 
     */
    public void setBackgroundColorTimerText(String color)
    {
    	timer_view.setBackgroundColor(Color.parseColor(color));
    }
    
    /**
     * Método que devuelve el objeto EditText que contiene el texto escrito por el jugador en el chat
     * @return Devuelve un objeto de la clase EditText que contiene el mensaje escrito en el chat
     */	
	public EditText getMessageText(){
		return message_text;
	}
	
	/**
     * Método que fija un texto dado en la pantalla de la conversación. Nota: puede añadirse etiquetas de color HTML
     * @param string Cadena en formato String que contiene el texto a fijar en pantalla.
     */
	public void setText(String string){
		
		if(string != null && this.text != null)
			this.text.setText(Html.fromHtml(string),TextView.BufferType.SPANNABLE);
	
	    scrollDown();
	}
	
	/**
     * Método que devuelve el texto que hay actualmente en la pantalla de la conversación
     * @return Devuelve en formato String el texto que hay actualmente en la pantalla
     */
	public String getText(){
		return text.getText().toString();
	}
	
	/**
     * Método para hacer desplazar la conversación hacia abajo
     */
	public void scrollDown(){
		if(sv != null)
		sv.post(new Runnable() { 
		    public void run() { 
		        sv.fullScroll(ScrollView.FOCUS_DOWN); 
		    } 
		});
	}
	
}
