package com.manuel.pfc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.res.Resources;

/**
 * Esta clase se encarga de gestionar las tareas del comisario, así como todos sus avances.
 * @author Manuel Jesús Pérez Zurera
 *
 */

public class Assignment {

	private List<Integer> initialnodes;
	private boolean[] checklist;
	private String[] taskscodes;
	private String[] tasks;
	private boolean[][] adjacencymatrix;
	
	private Random random;
	private String assignment_question;
	private int max;
	private int amount;
	private int assignment_number;
	private int actualposition=0;
	private int hostfails=0;
	private boolean endgame=false;
	
	private Resources res;
	
	/**
	 * Esta clase se encarga para guardar los objectos comunes a toda la aplicación
	 * @param res Los recursos pertenecientes a la Activity cargada actualmente
	 *
	 */
	public Assignment(Resources res){
		random = new Random();
		initialnodes= new ArrayList<Integer>();		
		
		//Asignamos los recursos
		this.res = res;
		
		//Recogemos el maximo numero de preguntas que hay
		int id= res.getIdentifier("max", "integer", "com.manuel.pfc");
		max= res.getInteger(id);
				
		//Elegimos una aleatoria entre ellas
		assignment_number=random.nextInt(max)+1;
			
		//Leemos si es secuencial la pregunta
		id= res.getIdentifier("initialnodes"+assignment_number, "string", "com.manuel.pfc");
		String initialstates=res.getString(id);
				
		int index;
		
		while((index = initialstates.indexOf(',')) != -1){ //mientras que haya nodos
			String node = initialstates.substring(0, index);
			
			initialstates = initialstates.substring(index+1, initialstates.length());
			initialnodes.add(Integer.parseInt(node)); //pasamos de string a entero	
		}	
		
		initialnodes.add(Integer.parseInt(initialstates)); //pasamos de string a entero	
				
		//Recogemos el texto a fijar en pantalla
		id= res.getIdentifier("assignment"+assignment_number+".1", "string", "com.manuel.pfc");
		assignment_question=res.getString(id);
				
		//Vemos la cantidad de respuestas por las que está formada
		id= res.getIdentifier("amount"+assignment_number, "integer", "com.manuel.pfc");
		amount=res.getInteger(id);
		
		adjacencymatrix = new boolean[amount+1][amount+1];
		
		//inicializo la matriz todo a falso
		for(int i=1; i<=amount; i++){
			for(int j=1; j<=amount; j++){
				adjacencymatrix[i][j]=false;
			}
		}
		
		for(int i=1; i<=amount; i++){
			//obtengo los nodos adyacentes de cada nodo
			id= res.getIdentifier("adjacency"+assignment_number+"."+i, "string", "com.manuel.pfc");
			String nodes=res.getString(id);
			
			System.out.println("Nodos: "+nodes);
			
			//Guardo sus adyacentes
			
			while((index = nodes.indexOf(',')) != -1){ //mientras que haya nodos
				//cojo la cadena desde 0 a index
				String node = nodes.substring(0, index);
				
				//corto la cadena
				nodes = nodes.substring(index+1, nodes.length());
					
				System.out.println("Insertado TRUE en: " +i+" "+ Integer.parseInt(node));
				adjacencymatrix[i][Integer.parseInt(node)]=true; //pasamos de string a entero y lo insertamos
			}
			
			//if(nodes.length()<2){ //si el tamaño es muy pequeño no hay coma	
				System.out.println("Insertado TRUE en: " +i+" "+ Integer.parseInt(nodes));
				adjacencymatrix[i][Integer.parseInt(nodes)]=true;
			//}
		}
				
		taskscodes = new String[amount+1];
		tasks = new String[amount+1];
		
		//Cargamos las respuestas en memoria
		for(int i=1; i<=amount; i++){
			id= res.getIdentifier("assignmentanswer"+assignment_number+"."+i, "string", "com.manuel.pfc");
			//las añadimos al array
			tasks[i]=res.getString(id);
			
			//añadimos los códigos
			id= res.getIdentifier("assignmentcode"+assignment_number+"."+i, "string", "com.manuel.pfc");
			taskscodes[i]= res.getString(id);
		}	
		
		for(String s: taskscodes){
			System.out.println("Task: " +s);
		}
		
		checklist = new boolean[amount+1];
		
		//marcamos que falta por hacerse 
		for(int i=1; i<=amount; i++){
			checklist[i]=false;
		}
	}
	
	/**
	 * Este método se encarga de buscar el código solicitado en la lista de objetos por encontrar
	 * @param code Código en formato String 
	 *
	 */
	public boolean search(String code){
		for(String s: taskscodes){
			if(s.equals(code)){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Este método se encarga de buscar el código final solicitado en la lista de objetos finales por encontrar
	 * @param code Código en formato String 
	 *
	 */
	public boolean checkFinalCode(String code)
	{
		int id = res.getIdentifier("assignmentcode"+assignment_number+".20", "string", "com.manuel.pfc");
		
		String finalcode = res.getString(id);
		
		if(code.equals(finalcode))
		{
			endgame=true;
			return true;
		}
		else
		{
			hostfails++;
			return false;
		}
	}
	
	/**
	 * Este método se encarga de devolver el texto perteneciente al código escaneado
	 * @param code Código en formato String 
	 *
	 */
	public String check(String code){ //comprobamos si está y si está la marcamos
		System.out.println("actualposition: " +actualposition);
		
		for(int index=1; index<=amount; index++){
			if(taskscodes[index].equals(code)){
				//Tenemos que comprobar si desde nuestra posición actual podemos ir a ese nodo
				System.out.println("Encontrado el código");
					
				//acabamos de empezar
				if(actualposition==0){ 
						
					//miramos los nodos iniciales
					for(int iterator=0; iterator<initialnodes.size(); iterator++){
						
						int initial=initialnodes.get(iterator);
						
						System.out.println("Posición inicial: " +initial + "Indice: " +index);
							
						//se puede ir desde un nodo inicial al nodo i
						if(initial==index){ 
							checklist[index]=true; //Lo anotamos
							actualposition=index;
							System.out.println("Actual position: " + actualposition);
							return tasks[index];
						}
				}
						
				}else{ //Estamos en una posición diferente a la inicial
					System.out.println("Matriz "+actualposition + " " + index);
					if(adjacencymatrix[actualposition][index]==true){
						checklist[index]=true; //Lo anotamos
						actualposition=index;
						System.out.println("Actual position: " + actualposition);
						return tasks[index];
					}
				}
			}
		}

		return null;
		
	}
	
	/**
	 * @return Devuelve true si se ha completado la lista, devuelve false en caso contrario
	 *
	 */
	public boolean checkComplete(){
		boolean completed=true;
		for(boolean b: checklist){ //con que una tarea esté sin terminar no estará completada
			if(b==false){
				completed=false;
			}
		}
		return completed;
	}
	
	/**
	 * @return Devuelve los indicios que el comisario puede leer actualmente en formato String
	 *
	 */
	public String getText(){
		//Cada bloque de indicios contiene dos indicios, si hemos analizado el nodo 1 o 2 ya debemos
		//cargar los indicios perteneciente al bloque 
		
		int bloque;
		
		//Si es impar tengo que sumarle uno
		if(actualposition%2 == 1)
			bloque = ((actualposition+1) / 2);
		else	
			bloque = (actualposition / 2);
		
		//El maximo son 12 pistas, osea 6 bloques, más de 6 bloques no vamos a cargar
		if(bloque>=6)
			bloque=5;
		
		System.out.println("Cargando bloque de Indicios: "+bloque);
		
		assignment_question = "";
		
		for(int i=0; i<=bloque;i++)
		{
			int id= res.getIdentifier("assignment"+assignment_number+"."+i, "string", "com.manuel.pfc");
			assignment_question+=res.getString(id);
		}
		
		return (assignment_question);
	}
	
}
