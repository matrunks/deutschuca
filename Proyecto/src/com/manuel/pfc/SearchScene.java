package com.manuel.pfc;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Clase que hereda de Activity y implementa el escuchador del tiempo. Esta clase se encarga de gestionar en su totalidad la actividad de encuentra el objeto
 * @author Manuel Jesús Pérez Zurera
 */

public class SearchScene extends FragmentActivity implements MediaPlayer.OnCompletionListener, TimerObserver{
	
	private boolean onTest;
	private boolean onFinish;
	private boolean saved;
	
	private MediaPlayer sound;

	private TimerClass timer;
	private TextView timer_view;
	
	private int actualquestion;
	private int questioncount=0;
	private int type;
	private int maxquestion;
	private int limit;
	private int secondslimit=60;
	
	private ArrayList<Integer> askedquestion;
	private ArrayList<Integer> sequence;
	private ArrayList<Integer> redButtons;
	private ArrayList<Integer> greenButtons;
	
	private String color;
	
	private Mark marks;
	private MediaPlayer success;
	private MediaPlayer error;
	private Random random;
	
	private Date end;
	private Date begin;
	
	/**
	 * Constructor de la clase, se le debe pasar un entero en el intent con el tipo de actividad
	 * 1 Para lugares. 2 Para objetos. 3 para características.
	 */
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_scene_layout);
		
		success = MediaPlayer.create(getApplicationContext(), R.raw.success);
		error = MediaPlayer.create(getApplicationContext(), R.raw.error);
		
		marks = new Mark();
		
		color = "gris";
		
		random = new Random();
		
		if(savedInstanceState != null){	//si no es la primera vez que arranco esta activity
			instantiateElements(savedInstanceState);
		}else{
			sequence = new ArrayList<Integer>();
			askedquestion = new ArrayList <Integer>();
			redButtons = new ArrayList <Integer>();
			greenButtons = new ArrayList <Integer>();
			
			saved = false;
			
			
			int id = getResources().getIdentifier("search_questionlimit", "integer", "com.manuel.pfc");
			
			limit = getResources().getInteger(id);
			
			//Cogemos el tipo, si es de tipo lugar o de armas
			Bundle bundle = getIntent().getExtras();
			type = bundle.getInt("type");
			
			Button b=(Button)findViewById(R.id.AcceptButton);
			b.setVisibility(View.VISIBLE);
			
			ImageView image=(ImageView)findViewById(R.id.image_professor);
			image.setVisibility(View.VISIBLE);
			
			TextView text = (TextView) findViewById(R.id.searchText2);
			text.setVisibility(View.VISIBLE);
			
			LinearLayout ll= (LinearLayout) findViewById(R.id.LinearLayout1);
			ll.setVisibility(View.GONE);
			
			text = (TextView) findViewById(R.id.searchText);
			text.setVisibility(View.VISIBLE);

			if(type == 1) //tipo lugares
			{
				id = getResources().getIdentifier("search_maxplaces", "integer", "com.manuel.pfc");	
				text.setText(getResources().getString(R.string.searchlabel1));
				setTitle(R.string.searchlabel1);
				
			}
			else if(type == 2) //tipo armas
			{
				id = getResources().getIdentifier("search_maxweapons", "integer", "com.manuel.pfc");
				text.setText(getResources().getString(R.string.searchlabel2));
				setTitle(R.string.searchlabel2);
			}
			else if(type == 3) //tipo apariencia de la cara
			{
				id = getResources().getIdentifier("search_maxfaces", "integer", "com.manuel.pfc");
				text.setText(getResources().getString(R.string.searchlabel3));
				setTitle(R.string.searchlabel3);
			}
			
			maxquestion = getResources().getInteger(id);
			
			//Mientras que no encontremos una pregunta no formulada
			
			do{
				actualquestion=obtenerInicial()+(random.nextInt(maxquestion)+1);
				//System.out.println(actualquestion);
			}while(askedquestion.contains(actualquestion));
			
			questioncount++;
			askedquestion.add(actualquestion);
			
			onTest=false;
			onFinish=false;
		}
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar 'Aceptar' en la presentación de la actividad
	 * Se encarga de inicializar la partida y cargar lo necesario en pantalla (imágenes y texto) para comenzar la partida.
	 * Una vez ejecutada esta función se considera que estamos en la partida y el tiempo empieza a correr.
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onAcceptClick(View button){
		if(onTest){ //Si hemos terminado
			onFinish=true;
			//Cargamos el resultado
			loadResult();
		}else {
			onTest=true;
			randomizePhotos();
			loadPhotos();
			
			//Activamos el begin time
			begin = new Date();
			begin.setTime(begin.getTime());
			
			//Activamos el end time
			end = new Date();
			end.setTime(end.getTime() + (secondslimit*1000));
			
			//Activamos el timer
			timer = new TimerClass(end);
			
			startTimerTask();
			reloadActivity();
		}
	}
	
	/**
	 * Método que como su nombre indica pone en marcha la tarea del reloj.
	 * El cronómetro comenzará a mandar avisos a través de los escuchadores inicializados.
	 */
	private void startTimerTask(){
		timer.start(this);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar un botón de las cuatro imágenes posibles, llama a la función que se encarga de comprobar si la opción es la correcta
	 * @param button Vista del botón que acabamos de pulsar
	 */
	public void onSelect1Click(View button){
		SelectAux(button, 0);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar un botón de las cuatro imágenes posibles, llama a la función que se encarga de comprobar si la opción es la correcta
	 * @param button Vista del botón que acabamos de pulsar
	 */
	public void onSelect2Click(View button){
		SelectAux(button, 1);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar un botón de las cuatro imágenes posibles, llama a la función que se encarga de comprobar si la opción es la correcta
	 * @param button Vista del botón que acabamos de pulsar
	 */
	public void onSelect3Click(View button){
		SelectAux(button, 2);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar un botón de las cuatro imágenes posibles, llama a la función que se encarga de comprobar si la opción es la correcta
	 * @param button Vista del botón que acabamos de pulsar
	 */
	public void onSelect4Click(View button){
		SelectAux(button, 3);
	}
	
	/**
	 * Método que se encarga de comprobar la validez del botón pulsado. Si el botón es el correcto, lo pondrá en verde, reproducirá el sonido del mismo y cargará la siguiente pregunta. En caso contrario pondrá en rojo el botón y reproducirá el sonido pero sin avanzar de pregunta.
	 * Anota la puntuación positiva o negativa dependiendo de si hemos acertado o no
	 * @param button Vista del botón que acabamos de pulsar
	 * @param button_number Entero que indica el número del botón que acabamos de pulsar.
	 */
	private void SelectAux(View button, int button_number){
		Button b = (Button) button;

		//Si hemos fallado
		if(checkAnwser(sequence.get(button_number))==false){
			//Lo añado a la lista de botones deshabilitados
			redButtons.add(button_number+1);	
			
			//Comenzamos el sonido de la foto
			startPhotoSound(button_number);
			
			//Ponemos en rojo el botón
			button.setBackgroundColor(Color.RED);
			
			//le cambio el texto
			b.setText("Falsch");
			
		}else{
			//Activamos greenButton a true
			greenButtons.add(button_number+1);
			
			//Ponemos el sonido de la imagen
			startPhotoSound(button_number);
			
			//Cambiamos el color del botón a verde
			button.setBackgroundColor(Color.GREEN);
			
			//Le cambio el nombre a Correcto
			b.setText("Richtig");
			
		}
	}
	
	/**
	 * Método que se encarga de comprobar si hemos llegado al final.
	 * Se ejecuta cada vez que pulsamos 'Send' es decir cada vez que respondemos a una pregunta. Si hemos superado el límite de preguntas cargará la función loadResult y dará por finalizada la actividad.
	 */
	private void checkEnd(){
		
		//Si hemos llegado al final
		if(questioncount>limit){
			onFinish=true;
			onTest=false;
			loadResult();
		}
		
		else{ //Si todavía quedan preguntas
			//Seleccionamos una nueva pregunta que no haya sido preguntada
			
			do{
				actualquestion=obtenerInicial()+(random.nextInt(maxquestion)+1);
				//System.out.println(actualquestion);
			}while(askedquestion.contains(actualquestion));
			
			//Añado la pregunta actual a las preguntadas
			askedquestion.add(actualquestion);
			
			//Creo una nueva secuencia de botones
			sequence = new ArrayList<Integer>();
			
			//Borramos la lista de los botones rojos
			redButtons = new ArrayList<Integer>();
			
			//Borramos la lista de los botones verdes
			greenButtons = new ArrayList<Integer>();
			
			//Seleccionamos las fotos aleatoriamente
			randomizePhotos();
			
			//Las cargamos en el orden anterior
			loadPhotos();
			
			//recargamos la actividad
			reloadActivity();
		}
	}
	
	/**
	 * Método que se encarga de comprobar la validez del botón pulsado. Si el botón es el correcto, lo pondrá en verde, reproducirá el sonido del mismo y cargará la siguiente pregunta. En caso contrario pondrá en rojo el botón y reproducirá el sonido pero sin avanzar de pregunta.
	 * Anota la puntuación positiva o negativa dependiendo de si hemos acertado o no
	 * @param button Vista del botón que acabamos de pulsar
	 * @param button_number Entero que indica el número del botón que acabamos de pulsar.
	 */
	private boolean checkAnwser(int buttonnumber){
		
		//Si la respuesta actual se corresponde con el botón pulsado
		if(actualquestion==buttonnumber){
			//Aumentamos el numero de cuestiones preguntadas
			questioncount++;
			
			//Añadimos el acierto
			marks.scoreCorrect();
			
			return true;
		}
		
		else{ //si no corresponde
			
			//anoto un fallo
			marks.scoreFail();
			
			return false;
		}
	}
	
	/**
	 * Método que reproduce el sonido de la imágen que le pasamos como parámetro
	 * @param number Entero que indica el número de la foto que queremos reproducir en el indice de los recursos.
	 */
	public void startPhotoSound(int number){
		//Al cometer un error ponemos el sonido de la foto
		
		int id = getResources().getIdentifier(getResources().getString(R.string.prefix)+"search_audio"+sequence.get(number), "raw", "com.manuel.pfc");
		sound = MediaPlayer.create(getApplicationContext(), id);
		
		System.out.println("search_audio"+sequence.get(number));
		
		//Comenzamos el sonido
		sound.start();
		
		//Desactivamos los botones
		disableButtons();
		
		//Activamos el escuchador
		sound.setOnCompletionListener(this);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón de sonido de la pregunta descriptiva de la actividad.
	 * La función reproducirá un sonido dependiendo del tipo de actividad en la que estemos y la pregunta en concreto.
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onQuestionSoundClick(View button){
		int id = getResources().getIdentifier(getResources().getString(R.string.prefix)+"search_question_audio"+(actualquestion), "raw", "com.manuel.pfc");
		sound = MediaPlayer.create(getApplicationContext(), id);
		sound.start();
		System.out.println(actualquestion);
	}
	
	/**
	 * Método que se encarga de poner las fotos de manera aleatoria y sin repetirlas
	 */
	private void randomizePhotos(){	
		ArrayList<Integer> numeros = new ArrayList <Integer>();
		
		for(int i=1; i<=4; i++){
			//Tenemos un vector de números de manera que guardo los numeros que van saliendo del 0 al 3
			//Si toca 0, es hora de poner la foto correcta en el vector
			
			int n=0;
			
			do{
				n=random.nextInt(4);
			}while(numeros.contains(n)); //mientras no salga un número que no esté ya dentro
			
			numeros.add(n);
			
			int aux = 0;
			
			//Si sale 0 metemos la foto original en el vector y si no está ya metida
			if(n == 0 && !sequence.contains(actualquestion))
			{
				sequence.add(actualquestion);
				System.out.println("añadido original "+actualquestion);
			}
			
			else //Caso contrario, vamos escogiendo una foto que no haya salido ya
			{
				do{
					aux=obtenerInicial()+(random.nextInt(maxquestion)+1);
				}while(sequence.contains(aux));
				
				sequence.add(aux);
				System.out.println("añadido falso "+aux);
			}
		}
		
		//Rellenamos el vector aleatoriamente
		/*for(int i=1;i<=4;i++){
			int aux;
			
			do{
				
				if((maxquestion-actualquestion)<4){
					aux=actualquestion-random.nextInt(4);
				}else{
					aux=random.nextInt(4)+actualquestion;
				}
				
			}while(sequence.contains(aux));
			
			//System.out.println("La foto: "+aux+" se insertará en: "+i);
			
			//la foto correspondiente con search_image+aux, será colocada en el orden dictado por random
			sequence.add(aux);
			//System.out.println("El indice de aux es"+sequence.indexOf(aux));
		}*/
	}
	
	/**
	 * Método que se encarga de cargar las fotos que están actualmente en la pregunta
	 */
	private void loadPhotos(){
		//Con este método cargamos las fotos en el orden en el que hemos obtenido gracias a randomizePhotos();
		int aux=sequence.get(0);
		int id;
		
		ImageView image = (ImageView) findViewById(R.id.image1);
		
		System.out.println("Cargando: search_image"+(aux));
		id = getResources().getIdentifier("search_image"+(aux), "drawable", "com.manuel.pfc");
		image.setImageResource(id);
		
		aux=sequence.get(1);
		
		image = (ImageView) findViewById(R.id.image2);
	    System.out.println("Cargando: search_image"+aux);
		id = getResources().getIdentifier("search_image"+(aux), "drawable", "com.manuel.pfc");
		image.setImageResource(id);
		
		aux=sequence.get(2);
		
		image = (ImageView) findViewById(R.id.image3);
		System.out.println("Cargando: search_image"+aux);
		id = getResources().getIdentifier("search_image"+(aux), "drawable", "com.manuel.pfc");
		image.setImageResource(id);
		
		aux=sequence.get(3);
		
		image = (ImageView) findViewById(R.id.image4);
		System.out.println("Cargando: search_image"+aux);
		id = getResources().getIdentifier("search_image"+(aux), "drawable", "com.manuel.pfc");
		image.setImageResource(id);
	}
	
	/**
	 * Método que sirve para recargar la actividad, pudiendo haber sido después de un giro de pantalla o simplemente al cambiar de pregunta
	 */
	private void reloadActivity(){
		//Ponemos el texto del tiempo
		timer_view = (TextView) findViewById(R.id.secondsTimerView);
		timer_view.setVisibility(View.VISIBLE);
		
		LinearLayout ll = (LinearLayout) findViewById(R.id.LinearLayout1);
		ll.setVisibility(View.VISIBLE);
		
		//Comienza la prueba y escondemos el accept button
		Button b=(Button)findViewById(R.id.AcceptButton);
		b.setVisibility(View.GONE);
		
		b=(Button)findViewById(R.id.soundQuestionButton);
		b.setVisibility(View.VISIBLE);
		
		//Restauro los botones a su estado
		restoreButton(1);
		restoreButton(2);
		restoreButton(3);
		restoreButton(4);
						
		//Escondemos la imagen del profesor
		ImageView image=(ImageView)findViewById(R.id.image_professor);
		image.setVisibility(View.GONE);
				
		TextView text = (TextView) findViewById(R.id.searchText);
		text.setVisibility(View.GONE);
				
		text = (TextView) findViewById(R.id.searchText2);
		text.setVisibility(View.GONE);
		
		ll = (LinearLayout) findViewById(R.id.head_layout);
		ll.setVisibility(View.VISIBLE);
		
		ll = (LinearLayout) findViewById(R.id.head_layout2);
		ll.setVisibility(View.VISIBLE);
		
		int id = getResources().getIdentifier("search_question"+actualquestion, "string", "com.manuel.pfc");
		String question = getResources().getString(id);
		
		text = (TextView) findViewById(R.id.placeText);
		text.setText(question);
	}
	
	/**
	 * Método que se encarga de mostrar correctamente la puntuación y las frases correctas en las que habíamos fallado.
	 * También para el cronómetro y comprueba mediante la clase DatabaseHelper si es necesario guardar la puntuación o no.
	 */
	private void loadResult(){
		//Paramos los sonidos para que no carguen más fotos
		if(sound != null)
		{
			sound.stop();
			sound.release();
		}
		
		//Cerramos el reloj
		timer.stop();
		
		//Quitamos el texto del tiempo
		timer_view = (TextView) findViewById(R.id.secondsTimerView);
		timer_view.setVisibility(View.GONE);
		
		LinearLayout ll = (LinearLayout) findViewById(R.id.LinearLayout1);
		ll.setVisibility(View.GONE);
		
		ImageView image=(ImageView)findViewById(R.id.image_professor);
		image.setVisibility(View.VISIBLE);
		
		//Cogemos el tiempo actual
		Date now = new Date();
		now.setTime(now.getTime());
		
		String time_taken = timer.differenceToString(begin, now, secondslimit);
		
		//Mostramos los aciertos
		TextView text =(TextView)findViewById(R.id.markCorrectText);
		text.setText(marks.printCorrects()+ "\n" +getResources().getString(R.string.time_taken1)+" "+time_taken+" "+getResources().getString(R.string.time_taken2));
		text.setVisibility(View.VISIBLE);
		
		//Si tenemos algún fallo lo mostramos	
		if(marks.getFails()!=0){
			text =(TextView)findViewById(R.id.markFailText);
			text.setText(marks.printFails());
			text.setVisibility(View.VISIBLE);
		}
		
		//Ponemos el texto de finalización
		text=(TextView)findViewById(R.id.searchText);
		text.setText(R.string.search_end);
		text.setVisibility(View.VISIBLE);
		
		//Escondemos la pregunta
		ll = (LinearLayout) findViewById(R.id.head_layout);
		ll.setVisibility(View.GONE);
		
		ll = (LinearLayout) findViewById(R.id.head_layout2);
		ll.setVisibility(View.GONE);
		
		//Escondemos el botón aceptar
		Button b=(Button)findViewById(R.id.AcceptButton);
		b.setVisibility(View.GONE);
		
		b=(Button)findViewById(R.id.soundQuestionButton);
		b.setVisibility(View.GONE);
		
		//Mostramos el botón de salir
		b=(Button)findViewById(R.id.ExitButton);
		b.setVisibility(View.VISIBLE);
		
		b=(Button)findViewById(R.id.RetryButton);
		b.setVisibility(View.VISIBLE);
			
		if(saved == false)
		{
			//Comprobamos como hemos puntuado con anterioridad
			if(DatabaseHelper.checkMarkInDB(1,type,this,marks.getCorrects(),marks.getFails()))
			{
				//Guardamos en la base de datos los resultados
				DatabaseHelper.saveToDB(1,type,this,marks.getCorrects(),marks.getFails(),time_taken);
				
				saved = true;
			}
		}
		
		//Si aún quedan por aprobar
		if(DatabaseHelper.checkRemaining(1, type, this) > 0)
		{
			showToast("Mach die Übung noch "+ DatabaseHelper.checkRemaining(1, type, this)+ " mal!");
		}
		//Si hemos aprobado pero ya tenemos todas aprobadas
		else if(DatabaseHelper.checkRemaining(1, type, this) == 0)
		{
			int activity;
			int type;
			
			if(DatabaseHelper.loadSaveGame(this) == 10)
			{
				showToast("Super! Weiter so! Mach Chat");
			}
			else
			{
				//Lo convierto a la actividad
				activity = (int) Math.ceil(((double) (DatabaseHelper.loadSaveGame(this))/3));
				type = (DatabaseHelper.loadSaveGame(this)%3);
				if (type == 0)
					type = 3;
				
				showToast("Super! Weiter so! Mach jetzt Übung "+  activity  +"."+ type +"!");
			}
		}
	}
	
	/**
	 * Método que se ejecuta automáticamente al poner a la activity en segundo plano
	 * Se encarga de calcular la diferencia de tiempo ocurrida mientras estaba, ya que si dejamos la aplicación en segundo plano no sigue contando el tiempo.
	 */
	public void onRestart(){
		super.onRestart();
		
		//Calculamos el tiempo que ha pasado mientras la app estaba parada
		/*Date now = new Date();
		now.setTime(now.getTime());
		
		//Hacemos la diferencia entre las dos fechas
		int diff = (int) ((end.getTime() - now.getTime())/1000);
		*/
		
		if(timer != null && end !=null)
		{
			int diff = TimerClass.getRemainingTime(end);
			
			System.out.println("Tiempo restante: "+diff);
			
			System.out.println("On restart ha activado un secundero "+diff);
			timer.stop();
			timer = new TimerClass(end);
			startTimerTask();
		}
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón atrás de Android
	 * Se encarga de resetear el cronómetro y de volver al menú
	 */
	public void onBackPressed(){
		if(timer !=null)
		{
			timer.stop();
		}
		
		System.out.println("Saliendo");
		
		Intent intent = new Intent();
		intent.setClass(this, MainActivity.class);
		
		if(sound != null){
			sound.release();
		}
		
		startActivity(intent);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón salir
	 * Se encarga de resetear el cronómetro y de volver al menú
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onExitClick(View button){
		if(timer != null)
			timer.stop();
		
		Intent intent = new Intent();
		intent.setClass(this, MainActivity.class);
		error.release();
		success.release();
		
		if(sound != null){
			sound.release();
		}
		
		startActivity(intent);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón repetir
	 * Se encarga de resetear el cronómetro y de empezar una nueva actividad
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onRetryClick(View button){
		//Cerramos el reloj
		if(timer != null)
			timer.stop();
		
		Intent intent = new Intent();
		intent.setClass(this, SearchScene.class);
		intent.putExtra("type", type);
		
		if(sound != null){
			sound.release();
		}
		
		startActivity(intent);
	}
	
	/**
	 * Método para cargar un estado anterior después de haberse producido un giro de la aplicación
	 * @param inState Objeto que guarda el estado de las variables antes de que se produjese el giro
	 */
	private void instantiateElements(Bundle inState){ //y aqui los recupero
		onTest=inState.getBoolean("onTest");
		onFinish=inState.getBoolean("onFinish");
		saved=inState.getBoolean("saved");
		askedquestion=inState.getIntegerArrayList("askedquestion");
		questioncount=inState.getInt("questioncount");
		maxquestion=inState.getInt("maxquestion");
		type=inState.getInt("type");
		limit=inState.getInt("limit");
		sequence=inState.getIntegerArrayList("sequence");
		redButtons=inState.getIntegerArrayList("redButtons");
		greenButtons=inState.getIntegerArrayList("greenButtons");
		actualquestion=inState.getInt("actualquestion");
		marks=inState.getParcelable("marks");
		end=(Date) inState.getSerializable("end");
		begin=(Date) inState.getSerializable("begin");
		
		if(end != null)
			timer = new TimerClass(end);
		
		if(greenButtons.size() != 0){
			checkEnd();
		}
		
		if(onFinish){
			loadResult();
		}
		else if(onTest){
			loadPhotos();
			reloadActivity();
			startTimerTask();
		}
	}
	
	/**
	 * Método que sirve para guardar el estado de la activity cuando se produce un giro de la pantalla
	 * @param outState Objeto de tipo Bundle que guardará el estado de las variables de la actividad.
	 */
	protected void onSaveInstanceState(Bundle outState) {
	    // Save the values you need from your textview into "outState"-object
	    super.onSaveInstanceState(outState);
	    
	    //guardamos las variables
	    outState.putBoolean("onTest", onTest);
	    outState.putBoolean("onFinish", onFinish);
	    outState.putBoolean("saved", saved);
	    outState.putInt("maxquestion", maxquestion);
	    outState.putInt("actualquestion", actualquestion);
	    outState.putInt("questioncount", questioncount);
	    outState.putInt("type", type);
	    outState.putInt("limit", limit);
	    outState.putIntegerArrayList("sequence", sequence);
	    outState.putIntegerArrayList("askedquestion", askedquestion);
	    outState.putIntegerArrayList("redButtons", redButtons);
	    outState.putIntegerArrayList("greenButtons", greenButtons);
	    outState.putSerializable("end", end);
	    outState.putSerializable("begin", begin);
	    outState.putParcelable("marks", marks);
	    
	    //por último paro el reloj
	    if(timer != null)
	    	timer.stop();
	}
	
	/**
	 * Método que sirve para devolver el botón designado por el entero pasado por parámetro a su estado natural (poder ser pulsado)
	 * @param button_number Entero que indica el botón que queremos restaurar
	 */
	private void restoreButton(int button_number){
		
		//Si había botones en rojo
		if(redButtons.size() != 0){
			for(int i = 0 ; i < redButtons.size(); i++){
				int id = getResources().getIdentifier("button"+redButtons.get(i), "id", "com.manuel.pfc");
				Button b=(Button)findViewById(id);	
				
				//Lo pongo en rojo
				b.setBackgroundColor(Color.RED);
				
				//Deshabilito el botón
				b.setEnabled(false);

				//Le pongo la palabra fallo
				b.setText("Ausfall");
			}
		}
		
		else{ //Si no los había los devuelvo a su estado inicial 
			int id = getResources().getIdentifier("button"+button_number, "id", "com.manuel.pfc");
			Button b=(Button)findViewById(id);
			
			//le pongo el aspecto por defecto
			b.setBackgroundResource(android.R.drawable.btn_default);
			
			//habilito el botón
			b.setEnabled(true);
			
			//le restauro su texto
			b.setText(getResources().getString(R.string.search_select));
		}
		
		//Si hay alguno verde, lo dejo verde
		if(greenButtons.size() != 0){
			for(int i = 0 ; i < greenButtons.size(); i++){
				int id = getResources().getIdentifier("button"+greenButtons.get(i), "id", "com.manuel.pfc");
				Button b=(Button)findViewById(id);	
				
				//Lo pongo en rojo
				b.setBackgroundColor(Color.GREEN);
				
				//Deshabilito el botón
				b.setEnabled(false);

				//Le pongo la palabra fallo
				b.setText("Korrigieren");
			}
		}
	}
	
	/**
	 * Método que desactiva los cuatro botones de la activity
	 */
	private void disableButtons(){
		for(int i=1; i<=4 ; i++){
			int id = getResources().getIdentifier("button"+i, "id", "com.manuel.pfc");
			Button b=(Button)findViewById(id);
			
			b.setEnabled(false);
		}
	}
	
	/**
	 * Método que devuelve la posibilidad a los botones no pulsados, de ser pulsados nuevamente
	 */
	private void enableButtons(){
		for(int i=1; i<=4 ; i++){
			//Si el botón no está en la lista roja o verde (valor -1), significa que no fue pulsado 
			//Debemos mostrarlo entonces
			if(redButtons.indexOf(i) == -1){
				int id = getResources().getIdentifier("button"+i, "id", "com.manuel.pfc");
				Button b=(Button)findViewById(id);
				
				b.setEnabled(true);
			}
		}
	}

	/**
	 * Método privado que servirá para poder calcular que numeración tienen los recursos de la actividad en concreto
	 * @return Devuelve el número por el que empiezan los recursos de la actividad
	 */
	private int obtenerInicial(){
		if(type == 2)
			return 100;
		else if (type == 3)
			return 200;
		else
			return 0;
	}
	
	/**
	 * Método que se ejecuta al terminar de reproducir un sonido, para avisarnos de que éste acaba de ser reproducido
	 * @param mp Sonido acabado de reproducir
	 */
	public void onCompletion(MediaPlayer mp) {
		enableButtons();
		
		if(greenButtons.size() != 0){
			//Comprobamos si hemos llegado al final
			checkEnd();
		}
		
	}
	
	/**
	 * Muestra un mensaje en pantalla con las notificaciones de Android
	 * @param toast Mensaje en formato String 
	 */
	public void showToast(final String toast)
	{
	    runOnUiThread(new Runnable() {
	        public void run()
	        {
	        	//Como no puedo decir que dure el doble, hago que salga dos veces
	        	for(int i = 0 ; i<2 ; i++)
	        		Toast.makeText(SearchScene.this, toast, Toast.LENGTH_LONG).show();
	        }
	    });
	}

	/**
	 * Método que se ejecuta cuando el cronómetro llega a su fin, se encarga de cargar el final de la partida indistintamente de lo que estemos haciendo
	 */
	public void notifyTimer() {
		runOnUiThread(new Runnable(){
			public void run()  
	     	{ 
				//hidePhoto();
				//Nos ha avisado el reloj de un evento, en mi caso será cerrar la app
				loadResult();
	     	} 
			
		});
    }
	
	/**
	 * Método que se encarga de avisar cada segundo de que hay que cambiar el reloj que aparece en la parte superior de la aplicación. 
	 * Cuando queden menos de diez segundos cambiará de color entre gris y rojo.
	 */
	public void notifySeconds() {
		runOnUiThread(new Runnable(){
			public void run()  
	     	{ 
				//Si hay menos de 10 segundos vamos cambiando el color del texto
				if(timer.getRemainingTime() < 10)
				{
					if(color.equals("gris")){
						timer_view.setBackgroundColor(Color.parseColor("#FF0000"));
						color="rojo";
					}
						
					else
					{
						timer_view.setBackgroundColor(Color.parseColor("#E6E6E6"));
						color="gris";
					}
				}
				
				//Actualizamos el text view
				timer_view.setText(getResources().getString(R.string.remaining_time)+" "+timer.getRemainingTimeToString());
				
				//Nos ha avisado el reloj de que ha pasado un segundo
				System.out.println(timer.getSecondsToString());
	     	} 
			
		});
    }

}
