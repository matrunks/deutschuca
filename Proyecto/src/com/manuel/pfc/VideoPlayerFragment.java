package com.manuel.pfc;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;
/**
 * Clase que extiende de Fragment y que gestiona la reprodución de los vídeos de las pistas.
 * Su función será la de mostrar en una ventana sencilla la reproducción de un vídeo con un botón de play para poder repetir.
 * @author Manuel Jesús Pérez Zurera 
 *
 */

public class VideoPlayerFragment extends Fragment implements OnErrorListener {

	private Uri videouri;
	private VideoView vv;
	private View view;
	private onVideoPlayerFragmentListener mCallback;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.video_layout,
				   container, false);
		setRetainInstance(true);
		
		vv = (VideoView) view.findViewById(R.id.VideoView);
		
		vv.setOnErrorListener(this);

		mCallback.onVideoPlayerFragmentFinish(true);
		return view;
	}
	
	/**
	 * Método que sirve para fijar la URI del vídeo que queremos reproducir mediante el nombre del vídeo
	 * @param videonumber Nombre del video que queremos fijar
	 * @return Devuelve true si el vídeo existe en los recursos de la aplicación y false en caso contrario
	 */
	public boolean setVideoUri(String videonumber)
	{
		int id = getResources().getIdentifier(getResources().getString(R.string.prefix)+"video"+videonumber, "raw", "com.manuel.pfc");
		
		System.out.println("ID: "+id);
		
		//si la id es igual a cero es que no existe
		if(id == 0)
		{
			return false;
		}
		else{
			videouri = Uri.parse("android.resource://" + view.getContext().getPackageName() + "/" 
				      + id);
			
			vv.setVideoURI(videouri);
			
			vv.setOnPreparedListener( new MediaPlayer.OnPreparedListener() {
				  @Override
				  public void onPrepared(MediaPlayer pMp) {
				   //use a global variable to get the object
					  System.out.println("Preparado!");
					  pMp.start();
				  }
				});
			
			return true;
		}	
	}
	
	/**
	 * Método para reproducir el vídeo en la vista de vídeo
	 */
	public void Play()
	{
		vv.start();
	}
	
	// Debemos implementar la interfaz del listener
    public interface onVideoPlayerFragmentListener {
        public void onVideoPlayerFragmentFinish(boolean finish);
    }
	
	 @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onVideoPlayerFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
	 
	 /**
	  * Método que sirve para gestionar errores automáticos de reproducción de Android
	  * @param mp Objeto MediaPlayer dle que se ha producido el error
	  * @param what Código de error producido
	  * @param extra Información extra sobre el error
	  */
	 public boolean onError(MediaPlayer mp, int what, int extra) 
	 {
	     if (what == 100)
	     {
	         vv.stopPlayback();
	         
	         System.out.println("Solventando error");
	         
	         vv.start();
	         //Intent inn = new Intent(HelloInterruptVideoStream.this,TabAct.class);
	         //startActivity(inn);
	     }
	     /*else if (what == 1)
	     {
	         pb2.setVisibility(View.GONE);
	         Log.i("My Error ", "handled here");
	         video_view.stopPlayback();
	         Intent inn = new Intent(HelloInterruptVideoStream.this,TabAct.class);
	         startActivity(inn);
	     }
	     else if(what == 800)
	     {
	         video_view.stopPlayback();
	         Intent inn = new Intent(HelloInterruptVideoStream.this,TabAct.class);
	         startActivity(inn);
	     }
	     else if (what == 701)
	     {
	         video_view.stopPlayback();
	         Intent inn = new Intent(HelloInterruptVideoStream.this,TabAct.class);
	         startActivity(inn);
	     }
	     else if(what == 700)
	     {
	         video_view.stopPlayback();

	         Toast.makeText(getApplicationContext(), "Bad Media format ", Toast.LENGTH_SHORT).show();
	         Intent inn = new Intent(HelloInterruptVideoStream.this,TabAct.class);
	         startActivity(inn);
	     }*/

	     else if (what == -38)
	     {
	    	 vv.stopPlayback();
	         
	         System.out.println("Solventando error 2");
	         
	         vv.start(); 
	     }
	     return false;
	 }
}
