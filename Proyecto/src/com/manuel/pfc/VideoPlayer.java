package com.manuel.pfc;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.VideoView;

/**
 * Clase que hereda de Activity cuyo propósito es mostrar un vídeo por pantalla.
 * Su función es la de poder reproducir un vídeo prefijado desde una de las opciones del menú, para así comprobar la compatibilidad del móvil con los ficheros de vídeo.
 * @author Manuel Jesús Pérez Zurera
 */

public class VideoPlayer extends Activity{
	
	private Uri videouri;
	private VideoView vv;
	
	/**
	 * Constructor de la clase que sirve para fijar el vídeo de prueba de la aplicación al objeto VideoView de la vista.
	 * Cuando se pulse el botón play se reproducirá
	 */
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_layout);
		
		Bundle bundle= getIntent().getExtras();
		
		String videostring = bundle.getString("VIDEO");
		
		if(videostring == null)
		{
			videostring = "1029";
		}
		
		vv = (VideoView) findViewById(R.id.VideoView);
		
		System.out.println("Cargando video "+videostring);
		
		int id = getResources().getIdentifier(getResources().getString(R.string.prefix)+"video"+videostring, "raw", "com.manuel.pfc");
		
		videouri = Uri.parse("android.resource://" + getPackageName() + "/" 
			      + id);
		
		vv.setVideoURI(videouri);
		
		vv.start();
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón play.
	 * Pasa a reproducir el vídeo fijado por el constructor
	 * @param button Vista del botón que acaba de ser pulsado.
	 */
	public void onPlay(View button)
	{
		vv.start();
	}
}
