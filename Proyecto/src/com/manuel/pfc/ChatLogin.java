package com.manuel.pfc;

import java.io.IOException;

import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.XMPPException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Clase que se encarga de recoger los datos por pantalla del login y enviarlos al servidor con ayuda de ChatHelper
 * @author Manuel Jesús Pérez Zurera
 */	

public class ChatLogin extends Activity{
	private PFC app;
	private String username;
	private String password;

	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		//Por usar asmack, smack para android, se necesita inicializar la libreria en concreto para
				//que se encargue de las conexiones de algunos paquetes, como los de MUC
		SmackAndroid.init(this);
		setContentView(R.layout.chat_login_layout);
		app = (PFC) getApplication();
		
		/*userName = (EditText) findViewById (R.id.username);
		password = (EditText) findViewById (R.id.password);
		*/
		
		TextView internet_warning = (TextView) findViewById(R.id.internet_warning);
		
		internet_warning.setTextColor(Color.rgb(200,0,0));
		
		try {
			if(app.getChatHelper().connect(getResources().getString(R.string.serverdomain), getResources().getInteger(R.integer.serverport)) == false)
			{
				showToast("No se ha podido establecer conexión con el servidor");
				
				Intent intent = new Intent();
				intent.setClass(this, MainActivity.class);
				startActivity(intent);
			}
			
			else
			{
				showToast("Conectado");
			}
		} 
		catch (XMPPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	/*
	 * 
	 *  On login click empieza el programa a loguear y habilitar las conexiones
	 * 
	 */
	
	/**
	 * Método que se ejecuta al pulsar en el botón login, intentará hacer inicio de sesión a través de los datos escritos por pantalla
	 * @param button Es la vista del botón que acabamos de pulsar
	 * @throws XMPPException
	 * @throws IOException
	 */
	public void onLoginClick(View button) throws XMPPException, IOException{
		EditText et = (EditText) findViewById(R.id.username);
		username= et.getText().toString();
		et = (EditText) findViewById(R.id.password);
		password= et.getText().toString();
		
		hideSoftKeyBoard();
		
		if(app.getChatHelper().login(username, password,getResources().getString(R.string.serverdomain), getResources().getInteger(R.integer.serverport)))
		{
			showToast("Has iniciado sesión");
			
			//Cambiamos la activity a ChatMain que se encargara de gestionar todo el chat y sus vistas
			Intent intent = new Intent(this, ChatMain.class);
			startActivity(intent);
		}
		else
		{
			showToast("El login es inválido");
		}
	}
	
	/*
	 * 
	 * GESTIÓN DE LOS BOTONES
	 * 
	 */

	/**
	 * Metodo que muestra un mensaje de notificación en la vista de Android
	 * @param toast Mensaje en formato String que se quiera mostrar por pantalla
	 */
	public void showToast(final String toast)
	{
	    runOnUiThread(new Runnable() {
	        public void run()
	        {
	        	Toast.makeText(ChatLogin.this, toast, Toast.LENGTH_SHORT).show();  
	        }
	    });
	}
	
	/**
	 * Método que se ejecuta al pulsar atrás, desconectará la aplicación del servidor
	 */
	public void onBackPressed() {
		app.getChatHelper().disconnect();
		
		Intent intent = new Intent();
		intent.setClass(this, MainActivity.class);
		startActivity(intent);
	}	
	
	/**
	 * Método que sirve para esconder el teclado después de cada login
	 */
	private void hideSoftKeyBoard(){
		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
		
		EditText et = (EditText) findViewById(R.id.password);
			imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
	}

	
}