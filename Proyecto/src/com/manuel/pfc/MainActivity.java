package com.manuel.pfc;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Clase que extiende de Activity y es la actividad principal encargada de mostrar el menú principal
 * Contiene la gestión de botones así como indicar el nombre del jugador
 * @author Manuel Jesús Pérez Zurera
 *
 */

public class MainActivity extends Activity {

	private Cursor c;
	
	/**
	 * Es el constructor de la actividad
	 * @param savedInstanceState La instancia que guarda el estado
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Texto donde va el nombre
		TextView userNameText = (TextView) findViewById(R.id.userNameText);
		
		//Cojo los extras que me han pasado donde debe estar el nombre
		Bundle bundle = new Bundle();
		bundle = getIntent().getExtras();
		
		//Si tengo el nombre lo coloco
		if(bundle!=null){
			String userName = bundle.getString("UserName");
			userNameText.setText(" "+userName);
		}else{
		//Si no tengo el nombre lo recupero de la base de datos
			String userName=manageUsername();
			userNameText.setText(" "+userName);
		}
		
		int lastscore = DatabaseHelper.loadSaveGame(this);
		
		System.out.println("lastscore "+lastscore);
		
		for (int i=1; i<=lastscore; i++)
		{
			activateButton(i);
		}
		
		int id = 0;
		
		//Entonces tenemos el botón chat desbloqueado
		if(lastscore == 10)
		{
			id = getResources().getIdentifier("btn_chat", "id", "com.manuel.pfc");
		}
		else
		{
			id = getResources().getIdentifier("activity_button"+(lastscore), "id", "com.manuel.pfc");
			
		}
		
		Button b=(Button)findViewById(id);
		
		b.setTextColor(Color.YELLOW);
		b.setTypeface(null,Typeface.BOLD);
		b.setTextScaleX((float) 1.3);
	}
	
	
	/**
	 * Devuelve la visibilidad al botón indicado en el menú
	 * @param i Es el índice del botón que queremos activar.
	 */
	private void activateButton(int i)
	{
		System.out.println("Activando boton "+i);
		int id=0;
		
		//Hasta el 9 son ejercicios
		if(i<10)
			id = getResources().getIdentifier("activity_button"+i, "id", "com.manuel.pfc");
		//El 10 es el botón del chat
		else if(i==10)
			id = getResources().getIdentifier("btn_chat", "id", "com.manuel.pfc");
		
		Button b=(Button)findViewById(id);
		
		b.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Método encargado de recuperar de la BD el nombre de usuario
	 * @return Devuelve en formato String el nombre de usuario
	 */
	private String manageUsername(){
		//Consultamos el nombre
		
		DatabaseHelper databaseHelper = new DatabaseHelper(this);
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		
		//Seleccionamos las columnas id y nombre
		String colum[]={"_id", "name"};
		
		//Hacemos un query a la tabla Usuarios seleccionando id y nombre
		c = db.query("users", colum, null,null,null,null,null);
		
		int id, name;
		id=c.getColumnIndex("_id");
		name=c.getColumnIndex("name");
		
		//Recorremos la BD
		c.moveToLast();
		
		//No hay ningún nombre en la BD
		if(c.getCount()<1){ 
			//Entonces le pongo el nombre de 'student'
			String s=(String) this.getText(R.string.name);
			return s;
		}else{
			String s=c.getString(name);
			return s;
		}
	}

	
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	/**
	 * Método encargado de cargar la activity Perfil
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onProfileClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, Profile.class);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar el intent de escaneo
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onScanClick(View button){
		try {
			new IntentIntegrator(this).initiateScan(); 
			//Intent intent = new Intent("com.google.zxing.client.android.SCAN");
			//intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
			//startActivityForResult(intent, 0);

		} 
		catch (ActivityNotFoundException exception) {  
				//Toast.makeText(viewGroup.getContext(), "Error", Toast.LENGTH_SHORT).show();  
		} 
	}
	
	/**
	 * Método que se ejecutará automáticamente cuando volvamos de un intento de escaneo.
	 * @param requestCode Código de la petición que le hemos pedido al intent
	 * @param resultCode Resultado del intento que hemos invocado en forma de entero
	 * @param data El intent que hemos invocado 
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {  
	    super.onActivityResult(requestCode, resultCode, data);  
	    
	    //Si pulsamos atrás no habremos escaneado nada, por lo tanto data es null
	    if(data != null)
	    {
	    	//if((requestCode == 0) && (resultCode == -1)) {  
	    	Intent intent = new Intent();
	    	intent.putExtra("VIDEO", data.getStringExtra("SCAN_RESULT"));
			intent.setClass(this,VideoPlayer.class);
			startActivity(intent);
	    	
	    //} else {  
	        //Toast.makeText(viewGroup.getContext(), "Error", Toast.LENGTH_SHORT).show();  
	    //} 
	    }
	    
	     
	} 
	
	/**
	 * Método encargado de cargar la activity Puntuación
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onScoreClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, ScoreViewer.class);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Rellena los lugares
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onFillPlacesClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, FillTheGaps.class);
		intent.putExtra("type", 1);
		
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Rellena los objetos
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onFillWeaponsClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, FillTheGaps.class);
		intent.putExtra("type", 2);
		
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Encuentra los lugares
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onSearchPlacesClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, SearchScene.class);
		intent.putExtra("type", 1);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Encuentra los objetos
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onSearchWeaponsClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, SearchScene.class);
		intent.putExtra("type", 2);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Encuentra las características
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onSearchFacesClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, SearchScene.class);
		intent.putExtra("type", 3);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Características escondidas
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onHiddenFacesClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, HiddenPicture.class);
		intent.putExtra("type", 3);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Rellena las características
	 * @param button La vista del botón que hemos pulsado
	 */	
	public void onFillFacesClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, FillTheGaps.class);
		intent.putExtra("type", 3);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Chat
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onChatClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, ChatLogin.class);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Lugares escondidos
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onHiddenPlacesClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, HiddenPicture.class);
		intent.putExtra("type", 1);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Objetos escondidos
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onHiddenWeaponsClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, HiddenPicture.class);
		intent.putExtra("type", 2);
		startActivity(intent);
	}
	
	/**
	 * Método encargado de cargar la activity Visor de videos
	 * @param button La vista del botón que hemos pulsado
	 */
	public void onVideoClick(View button){
		Intent intent = new Intent();
		intent.setClass(this, VideoPlayer.class);
		intent.putExtra("VIDEO", "1029");
		startActivity(intent);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar atrás en el dispositivo.
	 */
	public void onBackPressed() {
		//cierro la base de datos
		c.close();
		
		//Con �ste c�digo hago que si pulsamos el bot�n atr�s salgamos de la aplicaci�n en vez de retroceder por el historial
	    Intent intent = new Intent();
	    intent.setAction(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_HOME);
	    startActivity(intent);
	        return;
	    }
}
