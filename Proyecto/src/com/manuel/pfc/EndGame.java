package com.manuel.pfc;

import android.app.Activity;

/**
 * Clase que extiende Activity encargada de mostrar una vista con el resultado final del chat.
 * Puede mostrar una imágen de victoria o de derrota dependiendo del resultado de la partida.
 * @author Manuel Jesús Pérez Zurera
 * 
 */
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Clase que hereda de Activity que se encarga de cargar el final de una partida del modo chat. 
 * Su única función es mostrar por pantalla una foto de la chica ficticia salvada o ejecutada por el asesino ficticio.
 * @author Manuel Jesús Pérez Zurera
 *
 */

public class EndGame extends Activity{
	
	/**
	 * Constructor que inicializará la imágen dependiendo del entero que le hayamos pasado al intent como extra.
	 * 0 cargará foto de derrota. En otro cargará foto de victoria.
	 */
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.end_game_layout);
		
		//Cogemos el tipo, si es de tipo lugar o de armas
		Bundle bundle = getIntent().getExtras();
		int type = bundle.getInt("type");
		
		ImageView image = (ImageView) findViewById(R.id.ImageView);
		TextView text = (TextView) findViewById(R.id.TextView);
		
		//Derrota
		if(type == 0)
		{
			image.setImageResource(getResources().getIdentifier("victim", "drawable", "com.manuel.pfc"));
			
			String cadena = new String();
			cadena = "<p><strong><font size=\"20\" COLOR=\"RED\">" + getResources().getString(R.string.chat_game_over) + "</font></strong></p>";
			text.setText(Html.fromHtml(cadena), TextView.BufferType.SPANNABLE);
		}
		//Victoria
		else
		{
			image.setImageResource(getResources().getIdentifier("teacher", "drawable", "com.manuel.pfc"));
			
			String cadena = new String();
			cadena = "<p><strong><font size=\"20\" COLOR=\"#008D13\">" + getResources().getString(R.string.chat_end_game) + "</font></strong></p>";
			text.setText(Html.fromHtml(cadena), TextView.BufferType.SPANNABLE);
		}	
	}

}
