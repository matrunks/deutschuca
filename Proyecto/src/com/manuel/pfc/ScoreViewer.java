package com.manuel.pfc;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Clase que hereda de Activity y gestiona la vista de las puntuaciones guardadas
 * Mostrará por temario tres actividades, y por cada actividad varias puntuaciones hasta un máximo de tres
 * Muestra cuando, que puntuación y cuanto hemos tardado en resolver cada intento
 * @author Manuel Jesús Pérez Zurera
 */

public class ScoreViewer extends Activity{
	
	/**
	 * Constructor que inicializa la vista
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//archivo xml
		setContentView(R.layout.score_layout);
		
		int activity = 1;
		
		//Bucle que busque los resultados
		while (writeToTextView(activity) && activity < 10){
			activity++;
		}
	}
	
	/**
	 * Método encargado de pintar en el texto de la imágen las puntuaciones obtenidas de una actividad
	 * Si la actividad es la 1 o la 2, podrá llegar a mostrar hasta 3 puntuaciones. Si la actividad es la numero 3 mostrará como máximo dos puntuaciones.
	 * Colocará al lado de cada puntuación un tick si ha sido superada, o una x si no ha sido superada aún
	 * @param activity El número de la actividad que queremos mostrar
	 * @return	Devuelve true para que se ejecute antes en el bucle
	 */
	private boolean writeToTextView(int activity){
		DatabaseHelper databaseHelper = new DatabaseHelper(this);
		SQLiteDatabase db = databaseHelper.getReadableDatabase();
		
		//Seleccionamos las columnas id y nombre
		String colum[]={"_id", "activity", "corrects", "fails", "date", "time"};
		
		//El where será activity = numero de la actividad
		String where = DatabaseHelper.ACTIVITY + "=" + activity;
		Cursor c = db.query("marks", colum, where,null,null,null,null);
		
		int corrects_index, fails_index, date_index, time_index;
		
		//Nos movemos a la primera
		c.moveToFirst();

		int limit;
		if((activity%3) == 1 || (activity%3) == 2)
		{
			limit = 3;
		}
		else
		{
			limit = 2;
		}
		
		if(c.getCount() < limit)
		{
			limit = c.getCount();
		}
		
		
		//Si no hay registros en la actividad escribo sin registro
		if(limit<1){ 
			db.close();
			return true;
		}
		else
		{
			for(int i=0; i<limit; i++)
			{
				//Cogemos los índices
				corrects_index=c.getColumnIndex("corrects");
				fails_index=c.getColumnIndex("fails");
				date_index=c.getColumnIndex("date");
				time_index=c.getColumnIndex("time");
				
				//Rescatamos los valores
				int corrects=c.getInt(corrects_index);
				int fails=c.getInt(fails_index);
				String date = c.getString(date_index);
				String time = c.getString(time_index);
				
				//Cogemos la traducción de Date corrects y fails
				String datestring = getResources().getString(R.string.date);
				String correctstring = getResources().getString(R.string.correct);
				String failstring = getResources().getString(R.string.fail);
				String timestring = getResources().getString(R.string.time);
				
				
				//La nota de cada activity vendrá dada por el número de activity modulo 3 
				int nota = (i%3) + 1;
				
				System.out.println("Escribiendo "+activity+" "+nota);
				
				//Escribimos la fecha
				int id = getResources().getIdentifier("scoreDate"+activity+"."+nota, "id", "com.manuel.pfc");
				TextView text =(TextView)findViewById(id);	
				
				//text.setText(datestring+": "+date);
				text.setText(date);
				
				//Escribimos los valores correctos
				id = getResources().getIdentifier("scoreCorrects"+activity+"."+nota, "id", "com.manuel.pfc");
				text =(TextView)findViewById(id);	
				
				//text.setText(correctstring+": "+corrects);
				text.setText(""+corrects);
				
				//Escribimos los valores fallidos
				id = getResources().getIdentifier("scoreFails"+activity+"."+nota, "id", "com.manuel.pfc");
				text =(TextView)findViewById(id);	
				
				//text.setText(failstring+": "+fails);
				text.setText(""+fails);
				
				//Escribimos los valores del tiempo
				id = getResources().getIdentifier("scoreTime"+activity+"."+nota, "id", "com.manuel.pfc");
				text =(TextView)findViewById(id);	
				
				//Mostramos la imagen de correcto o fallado
				id = getResources().getIdentifier("scoreImage"+activity+"."+nota, "id", "com.manuel.pfc");
				ImageView image = (ImageView) findViewById(id);
				
				if((activity%3) == 1 || (activity%3) == 2)
				{
					if(corrects-fails >= 7)
					{
						id = getResources().getIdentifier("tick", "drawable", "com.manuel.pfc");
						image.setImageResource(id);
					}
					else
					{
						id = getResources().getIdentifier("x", "drawable", "com.manuel.pfc");
						image.setImageResource(id);
					}
					
				}
				else
				{
					if(corrects-fails >= 6)
					{
						id = getResources().getIdentifier("tick", "drawable", "com.manuel.pfc");
						image.setImageResource(id);
					}
					else
					{
						id = getResources().getIdentifier("x", "drawable", "com.manuel.pfc");
						image.setImageResource(id);
					}
				}
				
				//text.setText(timestring+": "+time);
				text.setText(time);
				
				//Nos movemos al siguiente punto
				c.moveToNext();
			}
			
			db.close();
			
			return true;
		}
	}
}
