package com.manuel.pfc;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Clase que gestiona el almacenamiento del Blog de notas del comisario
 * @author Manuel Jesús Pérez Zurera 
 *
 */

public class BlocData implements Parcelable{
	
	private ArrayList<String> notes;
	
	public BlocData()
	{
		initialize();
	}
	
	/**
	 * Constructor a partir de un objeto anterior
	 * @param in Objeto Parcel para construir un nuevo Blog de notas a partir del estado previo de uno anterior
	 * 
	 */
	public BlocData(Parcel in)
	{
		initialize();
		readFromParcel(in);
	}
	
	/**
	 * Método encargado de inicializar el objeto
	 * 
	 */
	public void initialize()
	{
		notes = new ArrayList<String>();
		
		//Llenamos los cuatro primeros con cadena vacía
		for(int i=0; i<4; i++)
		{
			notes.add("");
		}
	}
	
	/**
	 * Método que sirve para guardar una nota en el blog en una posición descrita
	 * @param note Nota en formato String que queremos guardar en el Blog
	 * @param noteindex Índice en formato entero que indica donde se va a guardar la nota
	 */
	public void saveNote(String note, int noteindex)
	{
		//Si tiene menos de 4 lo vamos insertando hasta que estén las cuatro, entonces sustituimos
		notes.set(noteindex, note);
	}
	
	/**
	 * Método que sirve para devolver la nota que queremos de la posición que solicitemos
	 * @param noteindex Índice en formato entero que indica en que posición está la nota que queremos
	 * @return Devuelve la nota pedida en formato String
	 */
	public String getNote(int noteindex)
	{
		return notes.get(noteindex);
	}
	
	/**
	 * Método que sirve para comprobar si existe una nota en el lugar indicado
	 * @param noteindex Índice en formato entero que indica en que posición está la nota que queremos comprobar
	 * @return Devuelve true si el tamaño del blog es inferior al índice solicitado
	 */
	public boolean existNote(int noteindex){
		return (notes.size() > noteindex);
	}

	
	public int describeContents() 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Método encargado de guardar en memoria el estado actual del objeto this
	 */
	public void writeToParcel(Parcel arg0, int arg1) 
	{
		// TODO Auto-generated method stub
		arg0.writeList(notes);
	}
	
	/**
	 * Método que sirve para rescatar el estado en memoria de un objeto anterior
	 */
	public void readFromParcel(Parcel in)
	{
		in.readList(notes, null);
	}
	
	/**
	 * Método que sirve para devolver el blog entero en formato String
	 * @return Devuelve lel blog completo en formato String
	 */
	public String getWholeBloc()
	{
		String bloc = new String();
		
		bloc += "Notizblock des Kommissars: \n";
		for(int i=0; i<notes.size(); i++)
		{
			bloc += "<br>Notiz "+i+": "+notes.get(i) +"</br>";
		}
		
		return bloc;
	}
}
