package com.manuel.pfc;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Clase que implementa Parcelable para poder guardar su estado en memoria.
 * Gestiona la puntuación de la partida que estemos jugando en ese momento, los fallos, los aciertos guarda las frases corrects y tiene métodos para mostrar la puntuación
 */

public class Mark implements Parcelable {
	
	private int corrects;
	private int fails;
	private int hints;
	private ArrayList <Integer>corrects_streak;
	private ArrayList <Integer>fails_streak;
	private ArrayList <Integer>sentences_number;
	private ArrayList <String> correct_sentences;
	private int corrects_streak_index;
	private int fails_streak_index;
	
	/**
	 * Constructor que inicializa el objeto
	 */
	public Mark(){
		initialize();
	}
	
	/**
	 * Constructor que inicializa el objeto a partir de un estado anterior de un objeto de la misma clase
	 * @param in Objeto que contiene el estado anterior a partir del cual se creará un nuevo objeto
	 */
	public Mark(Parcel in){
		initialize();
		readFromParcel(in);
	}
	
	/**
	 * Método que inicializa el objto, pone todas las variables de puntuación a cero
	 */
	private void initialize(){
		corrects=0;
		fails=0;
		hints=0;
		
		corrects_streak_index=0;
		fails_streak_index=0;
		
		corrects_streak= new ArrayList<Integer>();
		fails_streak= new ArrayList<Integer>();
		correct_sentences= new ArrayList<String>();
		sentences_number= new ArrayList<Integer>();
		
		corrects_streak.add(0);
		fails_streak.add(0);
	}
	
	/**
	 * Método observador que devuelve el número de preguntas que el usuario ha acertado
	 * @return Entero que indica el número de preguntas correctas que el usuario ha acertado
	 */
	public int getCorrects(){
		return corrects;
	}
	
	/**
	 * Método observador que devuelve el número de preguntas que el usuario ha fallado
	 * @return Entero que indica el número de preguntas que el usuario ha fallado
	 */
	public int getFails(){
		return fails;
	}
	
	/**
	 * Método que anota un punto correcto
	 */
	public void scoreCorrect(){
		corrects++;
	}
	
	/**
	 * Método que anota un fallo
	 */
	public void scoreFail(){
		fails++;		
	}
	
	/**
	 * Método que anota una pregunta en la que hemos fallado. Sólo la anotará la primera vez
	 * @param sentence Frase correcta que ha fallado el usuario
	 * @param number Número de la frase que ha fallado el usuario
	 */
	public void noteSentence(String sentence, int number){
		if(!sentences_number.contains(number)){
			correct_sentences.add(sentence);
			sentences_number.add(number);
		}
	}
	
	/**
	 * Devuelve en formato cadena los aciertos cometidos
	 * @return Cadena con el número de aciertos cometidos
	 */
	public String printCorrects(){
		return "Du hast "+corrects+" Punkte.";
	}
	
	/**
	 * Devuelve en formato cadena los fallos cometidos
	 * @return Cadena con el número de fallos cometidos
	 */
	public String printFails(){
		return "\nDu hast "+fails+" Fehler.";
	}
	
	/**
	 * Devuelve en formato cadena las cuestiones correctas para que el usuario pueda ver en que se ha equivocado
	 * @return Cadena que contiene las cuestiones correctas con colores HTML para diferenciar las partes importantes
	 */
	public String printCorrectSentences(){
		String result = new String();
		
		for(int i=0; i<correct_sentences.size(); i++){
			result = result + ("\n\n<p> <font COLOR=\"RED\"> Frage "+ sentences_number.get(i) +" ist nicht richtig.</font> </p>\n <p><font COLOR=\"#008D13\">Richtig wäre:</font> ");
			result = result + (correct_sentences.get(i) + "</p>");
		}
		
		result = result + "\n\n";
		
		return result;
	}

	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Método que guarda los valores del objeto en memoria para poder ser recreado de nuevo
	 * @param arg0 Objeto Parcel en el que se guardará en memoria el estado del objeto actual
	 */
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		arg0.writeInt(corrects);
		arg0.writeInt(fails);
		arg0.writeInt(hints);
		
		arg0.writeInt(corrects_streak_index);
		arg0.writeInt(fails_streak_index);
		
		arg0.writeList(corrects_streak);
		arg0.writeList(fails_streak);
		arg0.writeList(sentences_number);
		arg0.writeList(correct_sentences);
	}
	
	/**
	 * Método que lee del objeto pasado por parámetro el estado anterior del objeto y lo inicializa tal y como estaba
	 * @param in Objeto Parcel que contiene la información del estado anterior de objeto
	 */
	private void readFromParcel(Parcel in) {
		 corrects = in.readInt();
		 fails = in.readInt();
		 hints = in.readInt();
		 
		 corrects_streak_index = in.readInt();
		 fails_streak_index = in.readInt();
		 
		 in.readList(corrects_streak, null);
		 in.readList(fails_streak, null);
		 in.readList(sentences_number, null);
		 in.readList(correct_sentences, null);
    }

}
