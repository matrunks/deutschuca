package com.manuel.pfc;



import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.FormField;
import org.jivesoftware.smackx.MessageEventNotificationListener;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;

/**
 * Clase contenedora de Fragments que se encarga de poner en funcionamiento todas las vistas relativas al chat.
 * Debido a la arquitectura de Android ciertos aspectos no permiten ser transmitidos a otra activity, por lo cual esta clase sirve de punto de enlace entre las Activitys del chat.
 * @author Manuel Jesús Pérez Zurera
 *
 */

public class ChatMain extends FragmentActivity implements ChatConversationFragment.onChatConversationListener, ChatBuddyFragment.onChatBuddyListener, AssignmentFragment.onAssignmentFragmentListener, VideoPlayerFragment.onVideoPlayerFragmentListener, TimerObserver {
	private boolean login = false;
	private boolean onGroupChat = false;
	private boolean inRoom = false;
	private boolean onChat = false;
	private boolean host = false;
	private boolean onAlbum=false;
	private boolean onAssignment = false;
	private boolean onVideo = false;
	private boolean game_over=false;
	private String talkto;
	private String talktoname;
	private String room;
	private String roomname;
	private String roomserver="@conference.vps2367-cloud.comalis.net";
	private String server="@vps2367-cloud.comalis.net";
	private String buddyservername="@vps2367-cloud.comalis.net";
	private String color = "gris";
	
	private String seed;
	private String lastsender;
	private int backtries=0;
	private char separador='_';
	private String mymucusername;
	private MultiUserChat muc;
	private ArrayList<String> buddy_list;
	private PFC app;
	private ChatBuddyFragment buddy_fragment;
	private ChatConversationFragment conversation_fragment;
	private AssignmentFragment assignment_fragment;
	private VideoPlayerFragment video_player_fragment;
	private ConnectionListener connection_listener;
	
	private ArrayList<String> senders;
	
	public String conversationTAG="conversation";
	public String buddyTAG="buddy";
	public String assignmentTAG="assignment";
	public String albumTAG="album";
	public String videoTAG="video";
	
	private int secondslimit=1200;
	private TimerClass timer;

	private Date end;
	private Date begin;
	
	private String selected_video;
	private ArrayList<String> offline_messages;
	
	/**
	 * Método constructor de la clase. Se encarga de activar los escuchadores de conexiones para poder gestionarlos. También se puede recrear un objeto de la clase a partir de un estado anterior con el estado pasado por parámetro
	 * Inicializa la escucha de Invitaciones. Carga en memoria la lista de amigos. Activa la escucha de las conexiones de los amigos. Habilita el escuchado de conexiones.
	 * @param savedInstanceState Objeto Bundle dónde va almacenado el estado anterior del objeto que queremos recrear y arranca el escuchador de recibos de mensajes.
	 */
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat_main_layout);
		app=(PFC) getApplication();
		buddy_list= new ArrayList<String>();
		
		//Inicializamos los valores del servidor
		server= "@" + getResources().getString(R.string.buddyservername);
		roomserver= "@" + getResources().getString(R.string.roomserver);
		buddyservername= "@" + getResources().getString(R.string.buddyservername);
			
		if(savedInstanceState == null){// Entonces es la primera vez que se crea la activity
			
			talkto=new String();
			talktoname=new String();
			seed= new String();
			lastsender= new String();
			senders= new ArrayList<String>();

			buddy_fragment = new ChatBuddyFragment();
			offline_messages = new ArrayList<String>();
			
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.add(R.id.fragment_container, buddy_fragment).commit();
			
			//Arrancamos la escucha de invitaciones
			enableMultiChatInvitationListener();
			//Cargamos la lista de amigos
			loadBuddyList();
			//arrancamos el roster de la lista de amigos
			enableRoster(); 
			//Arrancamos el escuchador de conexion
			enableConnectionListener();
			//Arrancamos el contestador de recibos de mensajes
			enableMessageEventManager();	
			
		}else{ //Ya estaba arrancada de antes la lista de amigos se ha rotado el dispositivo o pausado la activity
			onAssignment=savedInstanceState.getBoolean("onAssignment");
			onChat=savedInstanceState.getBoolean("onChat");
			onAlbum=savedInstanceState.getBoolean("onAlbum");
			onVideo=savedInstanceState.getBoolean("onVideo");
			game_over=savedInstanceState.getBoolean("game_over");
			seed=savedInstanceState.getString("seed");
			lastsender=savedInstanceState.getString("lastsender");
			mymucusername=savedInstanceState.getString("mymucusername");
			roomname=savedInstanceState.getString("roomname");
			buddy_list=savedInstanceState.getStringArrayList("buddy_list");
			offline_messages=savedInstanceState.getStringArrayList("offline_messages");
			senders=savedInstanceState.getStringArrayList("senders");
			selected_video=savedInstanceState.getString("selected_video");
			
			/*if((onChat=savedInstanceState.getBoolean("onChat"))==true){
				talkto=savedInstanceState.getString("talkto");
				talktoname=savedInstanceState.getString("talktoname");
				seed=savedInstanceState.getString("seed");
			}*/
			if((onGroupChat=savedInstanceState.getBoolean("onGroupChat"))==true){
				room=savedInstanceState.getString("room");
				inRoom=savedInstanceState.getBoolean("inRoom");
				host=savedInstanceState.getBoolean("host");	
				muc = new MultiUserChat(app.getChatHelper().getConnection(), room);

				if(host == true && game_over == false)
				{
					end=(Date) savedInstanceState.getSerializable("end");
					begin=(Date) savedInstanceState.getSerializable("begin");
					
					timer = new TimerClass(end);
				}
			}
			instantiateFragments(savedInstanceState);
		}
	}
	
	/*
	 * 
	 * 
	 * 
	 * 										SECCIÓN ENABLE
	 * 
	 * 
	 * 
	 * 
	 */
	
	/**
	 * Método encargado de cargar la lista de amigos en memoria
	 */
	private void loadBuddyList(){
		//Si tenemos agregado a algún amigo al menos
		if(app.getChatHelper().displayConnectedList() != null)
		{
			for(String user: app.getChatHelper().displayConnectedList()){
				Presence presence = app.getChatHelper().getConnection().getRoster().getPresence(user);
				
				//Evitamos que se cuelgue si presencia nula
				if(presence.getStatus()!=null)
				{
					System.out.println(presence.getStatus());
					if(presence.getStatus().contains("available")) //solo lo voy a meter en mi lista de contactos
						updateBuddyList(user); //Si tiene puesto mi status
				}
				
			}	
		}
		
	}
	
	/**
	 * Método encargado de activar la escucha de la lista de amigos
	 */
	private void enableRoster(){
		System.out.println("Roster enabled");
		app.getChatHelper().getConnection().getRoster().addRosterListener(new RosterListener() {         
				@Override
				public void entriesAdded(Collection<String> arg0) {
					
					
				}
				@Override
				public void entriesDeleted(Collection<String> arg0) {
			
					
				}
				@Override
				public void entriesUpdated(Collection<String> arg0) {
				
					
				}
				@Override
				public void presenceChanged(Presence presence) {
					String user = presence.getFrom();
				    System.out.println("Presence changed: " + user.toString() + " " + presence);
				 
				   // Presence bestPresence = chat.connection.getRoster().getPresence(user);
				    if (presence.toString().contains("unavailable")){
				    	removeFromBuddyList(user);
				    }
				    else if(presence.toString().contains("available")) { //Si está conectado
				    	updateBuddyList(user); //lo añadimos
		        	} 
				}    
    	});
	}
	
	
	
	/**
	 * Método que se encarga de detectar las conexiones y sus estados.
	 * Si se produce una caída avisará por una notificación de android.
	 * En el momento en el que se recupera la conexión enviará los mensajes en cola que se hayan escrito durante la caída
	 */
	private void enableConnectionListener(){
		app.getChatHelper().getConnection().addConnectionListener(connection_listener = new ConnectionListener() {
			@Override
			public void connectionClosed() {
				System.out.println("Connection closed");
			}
			
			@Override
			public void connectionClosedOnError(Exception arg0) {
				System.out.println("Conexión perdida");
				
				showToast("Conexión caída");
				
				/*new Thread(){
					public void run(){
						//primero debemos cerrar las conexiones anteriores
						app.getChatHelper().getConnection().removeConnectionListener(connection_listener);
						app.getChatHelper().disconnect();
						
						System.out.println("Caída");
						
						//reconectamos
						reconnect();
					}
				};*/
			}

			@Override
			public void reconnectingIn(int arg0) {
				
			}

			@Override
			public void reconnectionFailed(Exception arg0) {
				System.out.println("Reconnection:failed");
			}

			@Override
			public void reconnectionSuccessful() {
				showToast("Conexion recuperada");
				
				System.out.println("Reconectado");
				
				app.getChatHelper().getConnection().addConnectionListener(connection_listener);
				
				if(inRoom==true) {
		    		onGroupChat=true;
		        	inRoom=true;
		        	
		        	//borramos la conversación anterior
		        	//app.resetMultiChatLog();
		        	muc = new MultiUserChat(app.getChatHelper().getConnection(), room);
		        	
				    try {
						muc.join(mymucusername);
					} catch (XMPPException e) {
						e.printStackTrace();
					}
				    
				    System.out.println("Joined again to "+room);
				    //host=false;
				    
				    //Enviamos los mensajes de la cola acumulados (no han sido confirmados sus recibos aún)
				    sendQueueMessages();
				    
				    //Enviamos los mensajes o
				    sendOfflineMessages();
		    	}
			}
			
		});
	}
	
	/**
	 * Método que se encarga de activar la escucha de los recibos de los mensajes.
	 * Es primordial para saber si un mensaje ha sido entregado y quitarlo de la cola de espera
	 */
	private void enableMessageEventManager(){
		app.getChatHelper().getMessageEventManager().addMessageEventNotificationListener(new MessageEventNotificationListener() {
	          public void deliveredNotification(String from, String packetID) {
	              System.out.println("The message has been delivered (" + from + ", " + packetID + ")");
	             
	              //Borramos el mensaje cuyo packetID sea idéntico de la cola
	              ArrayList<Message> queue = app.getChatHelper().getMessagesQueue();
	              
	              for(int i=0; i<queue.size(); i++){
	            	  if(queue.get(i).getPacketID().equals(packetID)){
	            		  //showToast("The message has been delivered ("+ queue.get(i).getBody() + " , " +queue.size() + " , " + from + ", " + packetID + ")");
	            		  queue.remove(i);
	            	  }
	              }
	          }
	    
	          public void displayedNotification(String from, String packetID) {
	              System.out.println("The message has been displayed (" + from + ", " + packetID + ")");
	          }
	    
	          public void composingNotification(String from, String packetID) {
	              System.out.println("The message's receiver is composing a reply (" + from + ", " + packetID + ")");
	          }
	    
	          public void offlineNotification(String from, String packetID) {
	              System.out.println("The message's receiver is offline (" + from + ", " + packetID + ")");
	          }
	    
	          public void cancelledNotification(String from, String packetID) {
	              System.out.println("The message's receiver cancelled composing a reply (" + from + ", " + packetID + ")");
	          }
	      });
	}
	
	
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 										EDICIÓN DE LA LISTA
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	/**
	 * Método que sirve para eliminar de la lista de amigos cargada en memoria a un usuario dado
	 * @param user El usuario que queremos eliminar de la lista de amigos en memoria.
	 */
	private void removeFromBuddyList(String user){
		String s=searchInBuddyList(user);
		buddy_list.remove(s);

		System.out.println("Se ha desconectado: "+s);
		
		//Esta parte no funciona bien!
		
		//Comprobamos si ese usuario al desconectarse se ha llevado la sala consigo
		/*if(host==false && inRoom==true){
			try {
		    	System.out.println(MultiUserChat.getRoomInfo(app.getChatHelper().getConnection(), room+roomserver));    
			} catch (XMPPException e) {
				System.out.println("Nuestra sala está destruida");
				app.resetMultiChatLog();
				muc.leave();
				System.out.println("Se terminó la sala");
				inRoom=false;
				loadBuddyFragment();
			} catch (IllegalStateException e){
				System.out.println("Nos hemos caído");
				reconnect=true;
			}
		}*/
		
	}
	
	/**
	 * Método que devuelve el nombre de un usuario que tengamos almacenado en la lista de amigos en memoria
	 * @param user Usuario que queremos buscar
	 * @return Devuelve el nombre del usuario que queremos buscar en la lista de amigos cargada en memoria, pero sin el '@servidor'. En caso contrario devuelve null
	 */
	private String searchInBuddyList(String user){
		int index;
		String username1=new String();
		if(user.contains("@")){//entonces recortamos la cadena
			index = user.toString().indexOf('@');
			username1=user.toString().substring(0,index);
		}else
			username1=user;
		
		for(String s:buddy_list){
			String username2=s.toString();
			if(username1.equals(username2)){
				return s;
			}
		}
		return null;
	}
	
	/**
	 * Método que actualiza la lista de amigos cargada en memoria. Si el usuario dado no estaba, se añade a la lista. En caso contrario no sucede nada
	 * @param user Usuario que queremos añadir a la lista
	 */
	private void updateBuddyList(final String user){
		if(searchInBuddyList(user)==null){ //Es decir, no estaba conectado, entonces lo añadimos
			String s = new String();
			
			int index = user.toString().indexOf('@');
			final String username = user.toString().substring(0, index);
			s=username.toString();
			
			System.out.println("Se ha conectado: "+s);
			
			buddy_list.add(s);
			
			/*if(inRoom == false)
			{
				try {
					searchBuddyRoom();
				} catch (XMPPException e) {
					e.printStackTrace();
				}
			}*/
		}
	}
	
	private void enableChat(){
		onChat=true;
	}

	/**
	 * Método encargado de cargar en la vista el fragmento de la conversación
	 */
	private void loadConversationFragment(){
		System.out.println("Cambiando a conversation fragment");
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		//hacemos una transición entre los dos fragments
		transaction.replace(R.id.fragment_container, conversation_fragment);
		transaction.addToBackStack(null);
		// Commit the transaction
		transaction.commit();
	}
	/**
	 * Método encargado de cargar en la vista el fragmento de los vídeos
	 */
	private void loadVideoPlayerFragment(){
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		//hacemos una transición entre los dos fragments
		transaction.replace(R.id.fragment_container, video_player_fragment);
		transaction.addToBackStack(null);
		// Commit the transaction
		transaction.commit();
	}
	
	/**
	 * Método encargado de cargar en la vista el fragmento de los objetivos y el blog de notas
	 */
	private void loadAssignmentFragment(){
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		//hacemos una transición entre los dos fragments
		transaction.replace(R.id.fragment_container, assignment_fragment);
		transaction.addToBackStack(null);
		// Commit the transaction
		transaction.commit();
	}
	
	/**
	 * Método encargado de cargar en la vista el fragmento de la sala antes de empezar a jugar
	 */
	private void loadBuddyFragment(){
		onGroupChat=false;
		onChat=false;
		System.out.println("Loading buddy fragment");
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		//hacemos una transición entre los dos fragments
		transaction.replace(R.id.fragment_container, buddy_fragment);
		//getSupportFragmentManager().popBackStack();
		transaction.addToBackStack(null);
		// Commit the transaction
		transaction.commit();
	}
	
	
	/**
	 * Método que se encarga de actualizar la conversación cargada en memoria principal de la conversación grupal que se está llevando a cabo. Message es el mensaje que se añadirá a la conversación. Dependiendo del emisor se cambiará el color del mensaje dependiendo del rol.
	 * @param message Mensaje que queremos añadir a la conversación actual cargada en memoria
	 */
	private void updateMultiChatTextValue(Message message){
		if(message!=null){
			//cojo el índice por el @ y corto la cadena y selecciono solo el usuario
			int index = message.getFrom().toString().indexOf('@');
			
			String text_msg=new String();
			String old_text=new String();
			String premensaje= "\n";
			
			if(app.getMultiChatLog()!=null){
				old_text=app.getMultiChatLog();
			}
			
			index = message.getFrom().toString().indexOf('/');
			String sender = message.getFrom().toString().substring(index+1, message.getFrom().toString().length());
			
			/*if(sender.length()>19){
				sender= sender.substring(0, 19);
			}*/
			
			//System.out.println("Cambiando lastsender a " + sender);
			
			//if(!lastsender.equals(sender)){
				//premensaje=sender.substring(0, 11);
			//}
			
			//lastsender=sender;
			
			//Si es demasiado largo el nick lo acortamos SOLO en el texto no en el sistema
			if(sender.length() > 12)
			{
				premensaje=sender.substring(0,12);
			}
			else
			{
				premensaje=sender;
			}
			
			
			if(text_msg != null && message!=null)
				text_msg+=premensaje+"<br>"+app.getTime()+": "+message.getBody().toString()+"</br>";
			
			if(!senders.contains(sender))
			{
				senders.add(sender);
			}
			System.out.println("Sender "+sender+ " Kommissar");
			System.out.println("Mensaje "+message.toString());
			//Si el mensaje es que se ha fallado una pista, se pone en rojo
			if(message.getBody().toString().contains(getResources().getString(R.string.wrong_lead)) && sender.toString().equals(getResources().getString(R.string.kommissar)))
			{
				text_msg = "<p><strong><font size=\"12\" COLOR=\"RED\">" + text_msg + "</font></strong></p>";
			}
			//Si el mensaje contiene una pista correcta
			else if(message.getBody().toString().contains("Ja, du hast Zeuge ") && sender.toString().equals(getResources().getString(R.string.kommissar)))
			{
				text_msg = "<p><strong><font size=\"12\" COLOR=\"#008D13\">" + text_msg + "</font></strong></p>";
			}
			//Si no entonces escribimos normal
			else
			{
				switch(senders.indexOf(sender))
				{
					case 0:
						text_msg = "<p><font COLOR=\"BLUE\">" + text_msg + "</font></p>";
						break;
					case 1:
						text_msg = "<p><font COLOR=\"#A300D0\">" + text_msg + "</font></p>";
						break;
					default:
						text_msg = "<p><font COLOR=\"BLACK\">" + text_msg + "</font></p>";
						break;
				}
			}
			
			System.out.println("Case "+senders.indexOf(sender));
			
			
			app.putMultiChatLog(old_text+text_msg);
			
			if(onGroupChat){
				conversation_fragment.setText(app.getMultiChatLog());
				//Esto es para deslizar hacia abajo el texto
         	    conversation_fragment.scrollDown();
			}
		}	
	}
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 								GESTIÓN DE LOS BOTONES
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón play de los vídeos. Reproduce el vídeo cargado en memoria principal.
	 * @param Button Vista del botón que se acaba de pulsar
	 */
	public void onPlay(View Button){
		video_player_fragment.Play();
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón del Blog del comisario. Se encarga de cargar la vista del blog.
	 * @param Button Vista del botón que se acaba de pulsar
	 */
	public void onBlocClick(View Button){
		//Dejamos puesto el mensaje de escribe aqui
		//conversation_fragment.getMessageText().setText("Notier bitte hier!");
		
		assignment_fragment = new AssignmentFragment();
		onAssignment=true;
		loadAssignmentFragment();
	}
	
	/**
	 * Método que se ejecuta automáticamente cuando pulsamos el botón Exit en el chat.
	 * Se encarga de avisar por pantalla de que la partida se ha acabado en caso de ser host.
	 * Reinicia la conversación almacenada en memoria y elimina los listener de conexiones y desconecta al usuario del servidor.
	 * @param Button
	 */
	public void onLogoutClick(View Button){
		//comprobamos si yo he creado una sala, entonces debo destruirla al pulsar el botón desconectar
		if(inRoom){
			if(host==true){
				try {
					muc.destroy(null, null);
					showToast("Partida terminada");
				}		
				catch (XMPPException e) {
					e.printStackTrace();
				}
			}
		}
		
		//borramos la conversacion ya que ha concluido
		app.resetMultiChatLog();
		
		app.getChatHelper().getConnection().removeConnectionListener(connection_listener);
		
		app.getChatHelper().disconnect();
		
		showToast("Desconectado");
		
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón de envío de mensaje. Se encarga de llamar al método de manejo de mensajes
	 * @param Button Vista del botón que se acaba de pulsar
	 */
	public void onSendClick(View Button){ //Enviamos un mensaje
		//Si hay algo escrito, lo envío
		if(conversation_fragment == null)
			loadConversationFragment();
		else
			sendMessage(conversation_fragment.getMessageText().getText().toString());
	}
	
	/**
	 * Método que se encarga de mandar los mensajes que estaban acumulados en cola
	 */
	private void sendQueueMessages(){	
		//Primero clono la cola para no crear un bucle y lanzar solo una vez cada mensaje
		//ya que sendMessage si no obtiene confirmación lo mete en la cola
		ArrayList<Message> queue = new ArrayList<Message>();
		
		for(int i=0; i<app.getChatHelper().getMessagesQueue().size(); i++)
		{
			queue.add(app.getChatHelper().getMessagesQueue().get(i));
		}
		
		//Vaciamos la cola original
		app.getChatHelper().getMessagesQueue().clear();
		
		//Si tenemos mensajes offline en cola, los mandamos
	    for(int i=0; i<queue.size(); i++)
	    {
	    	String message = queue.get(i).getBody();
	    	//showToast("Enviado mensaje de la cola "+i);
	    	
	    	try {
				app.getChatHelper().sendGroupMessage(message,room);
			} catch (XMPPException e) {
				e.printStackTrace();
			}
	    }
	    
	    setTextOnConversation((String) app.getMultiChatLog());
	    
	    //Borramos los mensajes sin conexión
	    queue.clear();
	}
	
	/**
	 * Método que se encarga de enviar los mensajes que tenemos acumulados por haber estado desconectados
	 */
	private void sendOfflineMessages(){
	    //Si tenemos mensajes offline en cola, los mandamos
	    for(int i=0; i<offline_messages.size(); i++)
	    {
	    	String message = offline_messages.get(i);
	    	System.out.println("Enviado mensaje fuera de conexión");
	    	
	    	try {
				app.getChatHelper().sendGroupMessage(message,room);
			} catch (XMPPException e) {
				e.printStackTrace();
			}
	    }
	    
	    setTextOnConversation((String) app.getMultiChatLog());
	    
	    //Borramos los mensajes sin conexión
	    offline_messages.clear();
	}
	
	/**
	 * Método que se encarga de enviar el mensaje a la sala de chat en la que estemos actualmente.
	 * También añadirá el mensaje escrito por el usuario a la vista de la conversación con el color definido por el rol del jugador.
	 * @param message Mensaje escrito por el usuario que se va a enviar.
	 */
	private void sendMessage(String message){
		String me = (String) getText(R.string.me);
		String text_msg = new String();
		String old_msg = new String();
		String premensaje = "\n";
		
		//Esta variable contendrá si se ha enviado o no el mensaje
		boolean sent = false;
		
		try {
			sent = app.getChatHelper().sendGroupMessage(message,room);
		} catch (XMPPException e) {
			e.printStackTrace();
		}
		
		//Si el mensaje se ha enviado correctamente y no contiene un código
		if(!message.contains("c0de_")){ //Si no es un código cifrado
			if(app.getMultiChatLog()!=null){ //Si no es null es que hay un registro de chat previo
				old_msg = app.getMultiChatLog();
			}
			
			//if(!lastsender.equals(me)){
				premensaje=me;
			//}
			
			//System.out.println("Cambiando lastsender a "+me);
			
			//lastsender=me;
			
			text_msg+=premensaje+"<br>"+ app.getTime() + ": "+message+"</br>";
			
			if(!senders.contains(me))
			{
				senders.add(me);
			}
			
			System.out.println("Me: "+ me);
			
			//Si el mensaje es que se ha fallado una pista, se pone en rojo
			if(message.toString().contains(getResources().getString(R.string.wrong_lead)) && host == true)
			{
				text_msg = "<p><strong><font size=\"12\" COLOR=\"RED\">" + text_msg + "</font></strong></p>";
			}
			//Si el mensaje contiene una pista correcta
			else if(message.toString().contains("Ja, du hast Zeuge ") && host == true)
			{
				text_msg = "<p><strong><font size=\"12\" COLOR=\"#008D13\">" + text_msg + "</font></strong></p>";
			}
			//Si no entonces escribimos normal
			else
			{
				switch(senders.indexOf(me))
				{
					case 0:
						text_msg = "<p><font COLOR=\"BLUE\">" + text_msg + "</font></p>";
						break;
					case 1:
						text_msg = "<p><font COLOR=\"#A300D0\">" + text_msg + "</font></p>";
						break;
					default:
						text_msg = "<p><font COLOR=\"BLACK\">" + text_msg + "</font></p>";
						break;
				}
			}
			

			System.out.println("Case "+senders.indexOf(me));
			
			app.putMultiChatLog(old_msg+text_msg);
			
			//Si el mensaje no se ha enviado lo ponemos a la cola
			if(sent == false)
			{
				offline_messages.add(message);
				showToast("Conexión NO establecida, se enviará el mensaje luego automaticamente");
				System.out.println("Añadido a la cola offline "+message);
			}
			
			//Si se ha enviado actualizamos el texto en pantalla
			else if(onGroupChat){
				conversation_fragment.setText((String) app.getMultiChatLog());
				System.out.println("Enviando MUC mensaje a la sala: "+room);
				conversation_fragment.scrollDown();
			}
			
			conversation_fragment.getMessageText().setText("");
		}
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón Scan.
	 * Se encarga de llamar al intent del escaneo de la librería Zxing, que abrirá la aplicación Barcode Scanner y nos proporcionará un código
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onScanClick(View button){
		if(!game_over)
		{
			try {
				new IntentIntegrator(this).initiateScan(); 
				//Intent intent = new Intent("com.google.zxing.client.android.SCAN");
				//intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
				//startActivityForResult(intent, 0);
			} 
			catch (ActivityNotFoundException exception) {  
					//Toast.makeText(viewGroup.getContext(), "Error", Toast.LENGTH_SHORT).show();  
			}
		}
	}
	
	
	/**
	 * Método que se ejecuta a la vuelta de la llamada de un Intent para escanear
	 * @param requestCode Código de la petición al intent para poderse diferenciar de distintas llamadas
	 * @param resultCode Código de resultado de la llamada al intent
	 * @param data Contiene los datos que acabamos de escanear
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {  
	    super.onActivityResult(requestCode, resultCode, data);  

	    //A veces se pulsa atrás para cancelar el intento de escaneo, por lo tanto data == null
	    if(data != null)
	    {
	    	//Cargamos la conversaci�n para poner un mensaje
	    	loadConversationFragment();
	    	
	    	//Lo primero es comprobar si somos host o no
	    	//game over debe ser false
	    	if(host==true && game_over==false)
	    	{
	    		String finalcode = data.getStringExtra("SCAN_RESULT");
	    		
	    		System.out.println("El objetivo es: "+finalcode);
	    		
	    		//Si el c�digo coincide con el asesino/victima
	    		if(app.getAssignment().checkFinalCode(finalcode))
	    		{
	    			//Han terminado la prueba
	    			loadEndGame();
	    		}
	    		else
	    		{
	    			sendMessage("<p><strong><font size=\"12\" COLOR=\"RED\">"+"Der Kommissar hat nicht den Mörder identifiziert." + "</font></strong></p>");
	    		}
	    		
	    	}
	    	else
	    	{
		    	//Codifico
		    	String encodedtext = codificar(seed+separador+roomname+separador+app.getChatHelper().getUsername()+server+separador+data.getStringExtra("SCAN_RESULT"));
		    	
		    	sendMessage("c0de"+separador+encodedtext);
		
		    	//Creamos el video player fragment
		    	video_player_fragment = new VideoPlayerFragment();

				//Cargamos el framgent del vídeo para reproducirlo
				loadVideoPlayerFragment();
				
				//Dejamos seleccionado el video
				selected_video = data.getStringExtra("SCAN_RESULT");
	    	}
	    }
	} 
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón Atrás de android
	 * Se encarga de cargar la vista inmediatamente anterior, o si estamos en la conversación del chat, a sacarnos de la partida después de cinco intentos.
	 */
	public void onBackPressed() {//Cuando pulsamos atrás, tenemos que tener en cuenta de:
		if(onAssignment==true || onAlbum==true || onVideo ==true){
			if(onAssignment)
				assignment_fragment.saveNotes();
			
			onAssignment=false;
			onAlbum=false;
			onVideo=false;
			
			onGroupChat=true;
			
			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
			transaction.replace(R.id.fragment_container, conversation_fragment);
			// Commit the transaction
			transaction.commit();
		}
		else if(onChat==true || onGroupChat==true){ //Si estamos chateando, aún no cerrar la sesión y mostrar la lista de amigos
			//añadimos un intento de ir hacia atrás
			backtries++;

			//Para salir del chat tenemos que pulsar 5 veces atrás, esto te enviaría a la lista de opciones de la lista de amigos
			if(backtries>=5){
				loadBuddyFragment();
				backtries=0;
			}
			else{ //Mostramos cuanto falta para salir
				if((5-backtries) == 4)
				{
					showToast("OJO! Se cerrará el chat!");
				}
				else
					showToast("Pulsa "+(5-backtries)+" vece(s) más para cerrar");
			}
				
		}else{
			//Si soy el host y he pulsado 5 veces atrás, cierro la sala
			if(host==true){
				try {
					muc.destroy(null, null);
					showToast("Partida terminada");
				} catch (XMPPException e) {
					e.printStackTrace();
				}
			}
			
			//Nos desconectamos
			app.getChatHelper().disconnect();
			
			if(timer != null)
			{
				timer.stop();
			}
			
			showToast("Desconectado");
			
			Intent intent = new Intent();
			intent.setClass(this, MainActivity.class);
			startActivity(intent);
		}
	}
	
	/**
	 * Método que se ejecuta al volver de un estado de parada de la aplicación. 
	 * Se encarga de que el medidor del cronómetro descuente los segundos que hemos estado con la aplicación en segundo plano y así evitar hacer trampas
	 */
	public void onRestart(){
		super.onRestart();
		
		//Si somos el comisario y no hemos terminado la partida aún, volvemos a poner en funcionamiento el reloj
		if(host == true && game_over == false)
		{
			int diff = TimerClass.getRemainingTime(end);
			
			System.out.println("Tiempo restante: "+diff);
			
			if(timer != null)
			{
				System.out.println("On restart ha activado un secundero "+diff);
				timer.stop();
				timer = new TimerClass(end);
				startTimerTask();
			}
		}
		
		
	}
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 									SECCIÓN MULTI-CHAT
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	
	/**
	 * Método que se encarga de activar la escucha de mensajes en la sala de chat.
	 * Diferenciará entre un mensaje codificado, y uno sin codificar que mostrará por pantalla.
	 */
	private void enableMultiChatMessageListener(){
		//if(MultiChatListenerEnabled==false){
			muc.addMessageListener(new PacketListener() {
	    	    public void processPacket(final Packet packet) 
	    	    {
	    	    	runOnUiThread(new Runnable() {  //necesitamos correrlo en un hilo ya que si no no se actualiza en tiempo real
	                    public void run() {
	                    	
	                    	
	                    	final Message message = (Message) packet;
	                    	//filtramos nuestros mensajes, cogemos la parte del usuario y comprobamos si somos nosotros
	                    	 if(message.getFrom().toString().contains("/")){
	                    		 int index = message.getFrom().toString().indexOf('/');
	                        	 String sendername = message.getFrom().toString().substring(index+1, message.getFrom().toString().length());
	                        	 System.out.println("Mensaje recibido: "+message.getBody().toString()+" de: "+message.getFrom().toString());
	                        	 System.out.println("Aux: "+sendername + "; mymucusername: " + mymucusername +";");
	                        	 
	                        	 if(!(sendername.toString().equals(mymucusername))){
	                        		//Debemos enviar una respuesta al recibo si no somos nosotros
	     	                    	/*if (DeliveryReceiptManager.hasDeliveryReceiptRequest(packet)) {
	     	                            DeliveryReceiptManager.processPacket(packet);
	     	                        }*/
	                        		 
	                        		 if(message.getBody().toString().contains("c0de_")){ //Nos han enviado un código 
		                        		 if(host && game_over == false){//y soy el coordinador y no ha terminado la partida
		                        			 checkQrCode(message.getBody()); 
		                        		 } 
		                        	 }else{
			    	     	    	        updateMultiChatTextValue(message);  
		                        	 }	
		                         }                        	
	                    	 }
	                    }
	                });      
	    	    }
			});	
			System.out.println("multi chat messages listener enabled for "+room);
		//}else{ //si ya está inicializada, recargamos el fragment
			//conversation_fragment=new ChatConversationFragment();
		//}
	}
	
	/**
	 * Método que se encarga de activar la escucha de invitaciones a salas.
	 * Si la sala en la que estoy tiene un sólo ocupante, es decir sólo estoy yo en la sala, la destruye y se une a la nueva que hemos recibido por invitación
	 */
	private void enableMultiChatInvitationListener(){
		
		MultiUserChat.addInvitationListener(app.getChatHelper().getConnection(), new InvitationListener() {
			@Override
			public void invitationReceived(Connection arg0, final String arg1,
					String arg2, String arg3, String arg4, Message arg5) {
				runOnUiThread(new Runnable() {  //necesitamos correrlo en un hilo ya que si no no se actualiza en tiempo real
	                public void run() {  
	                	
	                	//Hemos recibido una invitación, si nuestra sala actual está destruida entonces nos salimos y nos unimos a la nueva
	                	boolean roomIsDestroyed=true;
	                	
	                	System.out.println("Invitation received");
	                	
	                	try {
	                		roomIsDestroyed=false;
					    	System.out.println(muc.getRoomInfo(app.getChatHelper().getConnection(), room+roomserver));    
						} catch (XMPPException e) {
							roomIsDestroyed=true;
							onGroupChat=false;
							inRoom=false;
							System.out.println("Nuestra sala está destruida, aceptando invitación");
						}
	                	
	                	//También debemos mirar si yo estoy unido a una con dos personas, no me salgo
	                	if(inRoom){
	                		
	                		if(muc.getOccupantsCount()>1){
	                			roomIsDestroyed=false;
	                		}else{ //Solo estoy yo, la destruyo
	                			 try {
									muc.destroy(null,null);
								} catch (XMPPException e) {
									e.printStackTrace();
								}
	                			 roomIsDestroyed=true;
	                		}
	                	}
               	
	                	if(roomIsDestroyed==true){
	                		try {
								joinToRoom(arg1);
							} catch (XMPPException e) {
								e.printStackTrace();
							}
	                	}
	                }
	            }); 
			}
		});
		System.out.println("multi chat listener enabled");
	}
	
	/**
	 * Método que se ejecuta al pulsar el botón Comenzar partida en la vista de bienvenida.
	 * Se encarga de inicializar la conversación y unirse a una partida si hay alguna creada, o a crear una nueva y convertirnos en Comisario.
	 * @param button Vista del botón que se acaba de pulsar
	 * @throws XMPPException
	 */
	public void onMultiClick(View button) throws XMPPException{
		conversation_fragment = new ChatConversationFragment();
		if(inRoom==false){
			//cargamos el fragment de las conversaciones
			loadConversationFragment();
			
			room="test";
			String aux=room;
			boolean finishSearching=false;
			int i=1;
			
			//Buscamos si algún amigo está en sala
			searchBuddyRoom();

			//Si no nos hemos unido a ninguna, creamos una e invitamos
			if(onGroupChat==false){  
				host=true;
				
			    //Este bucle es importante: va buscando en el servidor, salas con el nombre del estilo room+i
			    //de manera que si recibe información de room1 es que ya existe y será de otro propietario
			    //y si no existe recibimos una excepción que nos dice que no se puede recuperar la información
			    //deberiamos recibir null pero no es así, así que lo más inteligente es cuando se produzca
			    //una excepción cerremos el bucle ya que si la sala no existe, tenemos hueco para crearla
			    while(finishSearching==false){
			    	aux=room+i;
			    	 try {
					    	System.out.println("Sala: "+MultiUserChat.getRoomInfo(app.getChatHelper().getConnection(), aux+roomserver).getRoom().toString());    
						} catch (XMPPException e) {
							finishSearching=true;
							System.out.println("Sala no encontrada, creandola");
						}
			    	 i++;
			    } 
			    
			    roomname=aux;
			    room=aux+roomserver;
			    
			    // Creo el multiuserchat
			    muc = new MultiUserChat(app.getChatHelper().getConnection(), room);

			    // Creo la sala y pongo mi nombre
			    try {
					muc.create("Kommissar"); //la idea es que este nombre sea anónimo para los usuarios de la conver
				} catch (XMPPException e) {
					e.printStackTrace();
				}
			    
			    System.out.println("Se procede a crear la sala "+room);
			    
			      // Get the the room's configuration form
			      Form form = muc.getConfigurationForm();
			      // Create a new form to submit based on the original form
			      Form submitForm = form.createAnswerForm();
			      // Add default answers to the form to submit
			      for (Iterator fields = form.getFields(); fields.hasNext();) {
			          FormField field = (FormField) fields.next();
			          if (!FormField.TYPE_HIDDEN.equals(field.getType()) && field.getVariable() != null) {
			              // Sets the default value as the answer
			              submitForm.setDefaultAnswer(field.getVariable());
			          }
			      }
			      
			      // Sets the new owner of the room
			      List owners = new ArrayList();
			      owners.add(app.getChatHelper().getUsername()+server);
			      System.out.println("A�adiendo "+app.getChatHelper().getUsername()+server+" a la lista de propietarios de "+room);
			      
			      //En la uca funciona con admins y en casa con owners
			      
			      //submitForm.setAnswer("muc#roomconfig_roomadmins", owners);
			      submitForm.setAnswer("muc#roomconfig_roomowners", owners); 
			      
			      // Send the completed form (with default values) to the server to configure the room
			      muc.sendConfigurationForm(submitForm);
			      
			      // Si envío una forma vacía, significa que quiero crear una sala instántanea y no privada
			      try {
					muc.sendConfigurationForm(new Form(Form.TYPE_SUBMIT));
				} catch (XMPPException e) {
					e.printStackTrace();
				}
			      
			    try {
					muc.join("Kommissar");
				} catch (XMPPException e) {
					e.printStackTrace();
				}
			    
			    //Activamos el begin time
				begin = new Date();
				begin.setTime(begin.getTime());
				
				//Activamos el end time
				end = new Date();
				end.setTime(end.getTime() + (secondslimit*1000));
			    
			    mymucusername=getResources().getString(R.string.kommissar);
			      
			    onGroupChat=true;
			    inRoom=true;
			    
			    app.putMultiChatLog((String) this.getText(R.string.muchost)+"\n");
				
				//Activamos los escuchadores referentes a los multi chats
			    enableMultiChatMessageListener();
			    
			    //Activamos el assigment
				app.initializeAssignment(getResources());
			    
				inviteToMultiChat();
				
				showToast(getResources().getString(R.string.chat_game_start));
			}
		}else{
			onGroupChat=true;
			loadConversationFragment();
		}
	}
	
	/*private void reconnect(){
		System.out.println("Reconectando");
		
		try {
			Intent newinIntent = new Intent(getApplicationContext(), ChatLogin.class);
			getApplicationContext().startService(newinIntent);
	

		} catch (Exception e) {
	        e.printStackTrace();
	        Intent newinIntent = new Intent(getApplicationContext(), ChatLogin.class);
	        getApplicationContext().stopService(newinIntent);
	    }
		
		showToast("Conectado");
		
		app.getChatHelper().login(app.getChatHelper().getUsername(), app.getChatHelper().getPassword(), getResources().getString(R.string.serverdomain), getResources().getInteger(R.integer.serverport));
	}*/
	
	//este método es bastante importante
	
	
	/**
	 * Método que sirve para buscar en el servidor si existe alguna sala creada por algún amigo
	 * si hacemos login, y alguien de nuestra lista está en sala, entramos con él
	 * Si pulsamos multichat, y da la casualidad de que no nos ha llegado la invitación, miramos si hay 
	 * sala ya creada y entramos, por tanto el usuario no hace nada y nos aseguramos de que aparezca en la sala
	 * si nos caemos, podemos volver a entrar, mientras que el dueño de la sala no se caiga
	 * @throws XMPPException
	 */
	private void searchBuddyRoom() throws XMPPException{
		Iterator<String> iterator=null;
		for(String buddy: buddy_list){ //recorremos la lista de amigos
			System.out.println("SearchBuddyRoom: "+buddy+buddyservername+"/Smack");
			iterator=MultiUserChat.getJoinedRooms(app.getChatHelper().getConnection(), buddy+buddyservername+"/Smack");
		}
		System.out.println("Iterator: "+iterator);
		if(iterator!=null){
			System.out.println("Iterator != null");
			while(iterator.hasNext()) { //Si hay alguna sala quiere decir que nuestro amigo está en una, nos unimos
		         joinToRoom(iterator.next().toString());
			}
		} 
	}
	
	/**
	 * Método que sirve para invitar al chat a los usuarios de mi lista de amigos
	 */
	private void inviteToMultiChat(){
		for(String user: buddy_list){
			System.out.println("Invitando a: "+user+buddyservername);
			muc.invite(user+buddyservername, "meet me");
		}
	}
	
	/**
	 * Método que se encarga de unirnos a una sala de chat y a preparar la conversación para ello.
	 * Codificará nuestro nombre y posteriormente nos unirá
	 * @param roomtojoin Nombre de la sala a la que queremos unirnos
	 * @throws XMPPException
	 */
	private void joinToRoom(String roomtojoin) throws XMPPException{
		conversation_fragment = new ChatConversationFragment();
		if(host==true){ //Teníamos una sala de antes, vamos a borrarla
			try {
				muc.destroy(null, null);
			} catch (XMPPException e) {
				e.printStackTrace();
			}
		}
		
		if(onGroupChat == false && inRoom == false) {
    		onGroupChat=true;
        	inRoom=true;
        	
        	//borramos la conversación anterior
        	app.resetMultiChatLog();
        	
        	room=roomtojoin;
        	int index = roomtojoin.indexOf('@');
        	roomname= roomtojoin.substring(0, index);
        	System.out.println("Roomname: " + roomname + "Room: "+room);
        	muc = new MultiUserChat(app.getChatHelper().getConnection(), room);
        	
        	loadConversationFragment();
        	
        	//Codificamos mi nombre en la sala
        	mymucusername= codificar(app.getChatHelper().getUsername());
        	mymucusername=mymucusername.substring(0, 12);
        	System.out.print("mymucusername: " +mymucusername+";");
        	mymucusername="Polizist"+mymucusername;
        	
        	System.out.println("Occupants count: "+MultiUserChat.getRoomInfo(app.getChatHelper().getConnection(), room).getOccupantsCount());
        	//Si hay uno solo, yo ser� el policia 1, y si hay 2, yo ser� policia 2
        	//mymucusername="Polizist"+MultiUserChat.getRoomInfo(app.getChatHelper().getConnection(), room).getOccupantsCount();
        	

        	System.out.println("Uniendome como "+mymucusername);
        	
		    try {
				muc.join(mymucusername);
			} catch (XMPPException e) {
				e.printStackTrace();
			}
		    
		    //Si no me consigo unir me cambio el nombre
		    while(!muc.isJoined())
		    {
		    	mymucusername+="1";	
		    	muc.join(mymucusername);
		    }
		    
		    String mucwelcome=(String) getResources().getText(R.string.mucwelcome);
        	app.putMultiChatLog(mucwelcome+" "+mymucusername+"\n");
		    
		    System.out.println("Joined to "+room);
		    host=false;
		    enableMultiChatMessageListener();
    	}
	}
	
	/*
	 * 
	 * 
	 * 
	 * 
	 *  							OTROS
	 * 
	 * 
	 * 
	 * 
	 */
	
	/**
	 * Método que se ejecuta al finalizar la carga de la vista de bienvenida
	 */
	public void onChatBuddyFinish(boolean finish) {
		if(seed!=null){//Ya teníamos la semilla descargada de antes
			buddy_fragment.activateButton();
			
			//Si estamos en sala, ponemos 'Continuar partida'
			if(inRoom)
			{
				buddy_fragment.setButtonName("Continuar partida");
			}
		}
	}
	
	
	/**
	 * Método que se ejecuta al finalizar la carga de la vista de la conversación
	 * Pone visible el tiempo y le resta la diferencia del tiempo que ha pasado desde que no estabamos en la vista
	 */
	public void onChatConversationFinish(boolean finish) {
		System.out.println("Conversation fragment cambiado");
		if(onGroupChat){
			conversation_fragment.setText((String) app.getMultiChatLog());
			conversation_fragment.scrollDown();
			
			//Si somos host debemos de ver el bloc de notas
			if(host == true){
				//Al ser el host tendremos el tiempo corriendo arriba
				conversation_fragment.setVisibileTimerView();
				
				//Si la partida aún no ha terminado
				if(game_over == false)
				{
					
					//Si había un reloj lo paro
					if(timer != null)
					{
						timer.stop();
					}
					
					int diff = TimerClass.getRemainingTime(end);
					
					System.out.println("Tiempo restante: "+diff);
					
					//Activamos el timer
					timer = new TimerClass(end);
					
					//Comenzamos el reloj de nuevo
					startTimerTask();
				}
				
				conversation_fragment.setVisibileBlocButton();
			}	
		}
	}
	

	/**
	 * Método que se ejecuta al finalizar la carga de la vista del Blog. Fija el contenido de los objetivos y del blog.
	 */
	public void onAssignmentFragmentFinish(boolean finish) {
		assignment_fragment.setText(app.getAssignment().getText());
		assignment_fragment.setBloc(app.getBloc());
	}
	
	/**
	 * Método que se ejecuta al finalizar la carga de la vista del vídeo.
	 * Se encarga de fijar el vídeo que se necesita reproducir
	 */
	public void onVideoPlayerFragmentFinish(boolean finish) {
		onVideo = true;
		
		if(video_player_fragment != null)
		{
			if(video_player_fragment.setVideoUri(selected_video)){
				//video_player_fragment.Play();
			}
			else
			{
				showToast("Código incorrecto");
				onBackPressed();
			}
		}
		//Han girado el móvil, se ha vuelto null el fragment del video, hay que crearlo de nuevo
		else
		{
			//Creamos el video player fragment
	    	video_player_fragment = new VideoPlayerFragment();

			//Cargamos el framgent del vídeo para reproducirlo
			loadVideoPlayerFragment();
		}
	}
	
	/*
	 * 
	 * 
	 * 							BLOC Y PISTAS																		
	 * 
	 * 
	 */
	
	/**
	 * Método que se ejecuta al pulsar el botón guardar.
	 * Nos lleva a la ventana de la conversación principal
	 * @param button Vista del botón que se acaba de pulsar
	 */
	public void onSaveClick(View button)
	{
		//Ya que cuando pulsamos atrás en el modo Bloc también se guarda,
		//El comportamiento de este botón debe ser idéntico al de salir hacia atrás
		onBackPressed();
	}
	
	
	
	/*
	 * 
	 * 
	 * 							MINIMIZAR O ROTAR PANTALLA
	 * 
	 * 
	 */
	
	
	/**
	 * Método que se encarga de guardar en una instancia los estados de las variables en el momento en el que se invoca.
	 * @param outState Bundle que guardará toda la información de las variables
	 */
	protected void onSaveInstanceState(Bundle outState) {
	    // Save the values you need from your textview into "outState"-object
	    super.onSaveInstanceState(outState);
	    
	    FragmentManager manager = getSupportFragmentManager();
	    if(conversation_fragment!=null) //si estamos en chat guardamos el fragmento de conversación
	    	manager.putFragment(outState, conversationTAG, conversation_fragment);
	    manager.putFragment(outState, buddyTAG, buddy_fragment);//Guardamos el fragmento de la lista
	    if(assignment_fragment!=null)
	    	manager.putFragment(outState, assignmentTAG, assignment_fragment);
	    if(video_player_fragment!=null)
	    	manager.putFragment(outState, videoTAG, video_player_fragment);
	    
	    //guardamos las variables
	    outState.putBoolean("onChat", onChat);
	    outState.putBoolean("onGroupChat", onGroupChat);
	    outState.putBoolean("onAssignment", onAssignment);
	    outState.putBoolean("onAlbum", onAlbum);
	    outState.putBoolean("onVideo", onVideo);
	    outState.putBoolean("inRoom", inRoom);
	    outState.putBoolean("host", host);
	    outState.putBoolean("game_over", game_over);
	    outState.putString("talkto", talkto);
	    outState.putString("talktoname", talktoname);
	    outState.putString("room", room);
	    outState.putString("seed", seed);
	    outState.putString("lastsender", lastsender);
	    outState.putString("mymucusername", mymucusername);
	    outState.putString("roomname", roomname);
	    outState.putStringArrayList("buddy_list", buddy_list);
	    outState.putStringArrayList("offline_messages", offline_messages);
	    outState.putStringArrayList("senders", senders);
	    outState.putString("selected_video", selected_video); 
	    
	    //Paramos el reloj al girar ya que crearemos uno nuevo
	    if(host == true && game_over == false){
			 outState.putSerializable("end", end);
			 outState.putSerializable("begin", begin);
	    	 timer.stop();
	    }
	    	
	}
	
	/**
	 * Método que sirve para reconstruir un objeto ChatMain después de que se produzca un giro de pantalla a partir de un objeto Bundle
	 * @param inState Objeto Bundle que guarda la información del objeto anterior en la memoria.
	 */
	private void instantiateFragments(Bundle inState) {
		   FragmentManager manager = getSupportFragmentManager();
		   FragmentTransaction transaction = manager.beginTransaction();
		   if (inState != null) { //si ya existía de antes unos fragmentos los recupero	   
			   buddy_fragment = (ChatBuddyFragment) manager.getFragment(inState, buddyTAG);
			   assignment_fragment = (AssignmentFragment) manager.getFragment(inState, assignmentTAG);
			   conversation_fragment = (ChatConversationFragment) manager.getFragment(inState, conversationTAG);
		   } else { //Si no, miro en que modo estaba
			  if(onChat || onGroupChat){
				  conversation_fragment = new ChatConversationFragment();
			      transaction.add(R.id.fragment_container, conversation_fragment, conversationTAG);
			      transaction.commit();
			  }else if (onAssignment){
				  assignment_fragment = new AssignmentFragment();
			      transaction.add(R.id.fragment_container, assignment_fragment, assignmentTAG);
			      transaction.commit();
			  }else if (onVideo){
				  video_player_fragment = new VideoPlayerFragment();
			      transaction.add(R.id.fragment_container, video_player_fragment, videoTAG);
			      transaction.commit();
			  }
			  else{
				  buddy_fragment = new ChatBuddyFragment();
			      transaction.add(R.id.fragment_container, buddy_fragment, buddyTAG);
			      transaction.commit();
			  }
		   }
	}
	
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 						Codificación y decodificación
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	/**
	 * Método que sirve para comprobar un código Qr.
	 * Para que un código se considere válido dentro de la aplicación, debe de contener la semilla utilizada por los jugadores, el nombre de la sala y debe ser enviado por alguien de mi lista de amigos.
	 * Todo el mensaje irá además codificado en base64 para evitar su lectura / manipulación
	 * @param message
	 */
	private void checkQrCode(String message){
		//Si estamos jugando aún
		if(game_over == false)
		{
			String clearcode = new String();
			String item;
			int index = message.toString().indexOf(separador); //Quito el prefijo c0de_
			String code = message.toString().substring(index+1,message.toString().length());

			//decodifico el código
			code = decodificar(code);
			
			if(code.contains(seed)){
				System.out.println("La semilla es la correcta: " + code);
				index = code.indexOf(separador);
				code = code.substring(index+1, code.length());
				
				if(code.contains(roomname)){
					System.out.println("La sala es la correcta: " + code);
					
					index = code.indexOf(separador);
					code = code.substring(index+1, code.length());
					
					for(String buddy: buddy_list){
						if(code.contains(buddy)){
							System.out.println("El usuario está en mi lista de amigos: " + code);
							
							index = code.indexOf(separador);
							clearcode = code.substring(index+1, code.length());
						}
					}
				}
			}
			
			System.out.println("Recibido código encriptado: "+clearcode);

			if((item=app.getAssignment().check(clearcode))!=null){
				sendMessage(item);
			}else{
				
				sendMessage(getResources().getString(R.string.wrong_lead));
			}
		}
	}
	
	/**
	 * Método auxiliar para codificar una frase dada en base64
	 * @param phrase Frase que queremos codificar
	 * @return Devuelve la frase dada codificada en base64
	 */
	private String codificar(String phrase){
		 // Sending side
		 byte[] data = null;
		 
		 try {
			data = phrase.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		 String encodedphrase = Base64.encodeToString(data, Base64.DEFAULT);
		 
		 return encodedphrase;
	}
	
	/**
	 * Método que sirve para decodificar una frase dada.
	 * @param encodedphrase La frase codificada que queremos decodificar
	 * @return Devuelve la frase decodificada para que se pueda manipular correctamente.
	 */
	private String decodificar(String encodedphrase){
		
		byte[] data = Base64.decode(encodedphrase, Base64.DEFAULT);
		
	    String phrase = null;
	    
	    try {
	        phrase = new String(data, "UTF-8");
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    }
	    
	    return phrase;
	}
	
	/*
	 * 
	 * 
	 * 					MÉTODOS ESTÁTICOS PARA MODIFICAR LA VISTA DESDE OTRO HILO 
	 * 
	 * 
	 * 
	 */
	
	/**
	 * Método auxiliar para mostrar notificaciones por pantalla
	 * @param toast Mensaje que queremos mostrar por pantalla
	 */
	public void showToast(final String toast)
	{
	    runOnUiThread(new Runnable() {
	        public void run()
	        {
	            Toast.makeText(ChatMain.this, toast, Toast.LENGTH_SHORT).show();
	        }
	    });
	}
	
	/**
	 * Método auxiliar para fijar texto a la conversación del chat
	 * @param text Texto que queremos fijar a la conversación del chat.
	 */
	public void setTextOnConversation(final String text)
	{
		 runOnUiThread(new Runnable() {
	        public void run()
	        {
	        	conversation_fragment.setText((String) text);
	    		System.out.println("Enviando MUC mensaje a la sala: "+room);
	    		conversation_fragment.scrollDown();
	        }
	    });
	}
	
	/**
	 * Método que se encarga de cargar la victoria de la partida.
	 * Enviará un mensaje con el contenido del Blog de notas del comisario a todos los usuarios y el tiempo que ha llevado realizar la actividad.
	 * También carga para el comisario una pantalla mostrando que la victima ficticia se ha salvado
	 */
	private void loadEndGame()
	{
		timer.stop();
		
		//Cogemos el tiempo de ahora
		Date now = new Date();
		now.setTime(now.getTime());
		
		sendMessage(getResources().getString(R.string.end_game) + "<br>"+ "<p><strong><font size=\"10\" COLOR=\"#008D13\">" +app.getBloc().getWholeBloc() + "</br>" + "<br><br>Tiempo utilizado: "+TimerClass.differenceToString(begin,now,secondslimit)+"</strong></font> </br></br>");
		
		//Activamos el game over y ya no podemos manejar códigos
		game_over=true;
		
		Intent intent = new Intent();
		intent.setClass(this, EndGame.class);
		intent.putExtra("type", 1);
		
		startActivity(intent);
		
	}
	
	/**
	 * Método que se encarga de cargar el fracaso de la partida.
	 * Enviará un mensaje a todos diciendo que se ha acabado el tiempo
	 * También carga para el comisario una pantalla mostrando que la victima ficticia no se ha salvado
	 */
	private void loadGameOver()
	{
		timer.stop();
		conversation_fragment.setTimerText(getResources().getString(R.string.remaining_time)+" "+"00:00");
		sendMessage("<p><strong><font size=\"10\" COLOR=\"RED\">" + "Se ha acabado el tiempo" +"</strong></font>");
		
		//Activamos el game over y ya no podemos manejar códigos
		game_over=true;
		
		Intent intent = new Intent();
		intent.setClass(this, EndGame.class);
		intent.putExtra("type", 0);
		
		startActivity(intent);
	}
	
	/**
	 * Método que activa el cronómetro
	 */
	private void startTimerTask(){
		timer.start(this);
	}

	/**
	 * Método que se ejecuta al acabar el cronómetro, cargará la función loadGameOver
	 */
	public void notifyTimer() {
		runOnUiThread(new Runnable(){
			public void run()  
	     	{ 
				//Nos ha avisado el reloj de un evento, en mi caso será cerrar la app
				System.out.println("End game");
				
				loadGameOver();
	     	} 
			
		});
    }
	
	/**
	 * Método que se ejecuta cada segundo que avise el cronómetro.
	 * Se encarga de actualizar el cronómetro que aparece en la vista de la conversación y a alternar de color entre gris y rojo cuando queda menos de un minuto.
	 */
	public void notifySeconds() {
		runOnUiThread(new Runnable(){
			public void run()  
	     	{ 
				//Si hay menos de 10 segundos vamos cambiando el color del texto
				if(timer.getRemainingTime() < 60)
				{
					if(color.equals("gris")){
						conversation_fragment.setBackgroundColorTimerText("#FF0000");
						color="rojo";
					}
						
					else
					{
						conversation_fragment.setBackgroundColorTimerText("#E6E6E6");
						color="gris";
					}
				}
				
				//Actualizamos el text view
				conversation_fragment.setTimerText(getResources().getString(R.string.remaining_time)+" "+timer.getRemainingTimeToString());
				
				//Nos ha avisado el reloj de que ha pasado un segundo
				System.out.println(timer.getSecondsToString());
	     	} 
			
		});
    }

}
