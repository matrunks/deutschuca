package com.manuel.pfc;


import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.text.format.Time;

/**
 * Clase encargada de manejar el tiempo del cronómetro en diversas actividades de la aplicación.
 * Tiene dos cronómetros internos, uno que avisará transcurrido cada segundo y otro que avisará cuando haya llegado la fecha fin proporcionada por el constructor
 * @author Manuel Jesús Pérez Zurera
 */

class TimerClass {
	private int seconds;
	private TimerTask reached_limit_time;
	private TimerTask seconds_signal;
	private Timer timer;
	private Timer secondstimer;
	private Date when;
	
	/**
	 * Constructor que toma como parámetro una fecha para avisarnos de que ya ha sucedido.
	 * Si ponemos una fecha dentro de un minuto, avisará por cada segundo transcurrido y a los 60 segundos dará un aviso especial y se parará el cronómetro
	 * @param date Fecha en la que se avisará cuando transcurra.
	 */
	public TimerClass(Date date){
		seconds=0;
		this.when=date;
	}
	
	/**
	 * Método que prepara la escucha de los avisos por cada segundo transcurrido (para saber que ha pasado un segundo) y la escucha del aviso único de que la fecha fin se ha cumplido ya. 
	 * @param timerobserver Escuchador por el cual mandaremos los avisos de tiempo ocurridos.
	 */
	public void start(final TimerObserver timerobserver){
		//Tarea para marcar el tiempo limite
		reached_limit_time = new TimerTask() 
	     {
			public void run() {
				timerobserver.notifyTimer();
			}
	     }; 
	     
	     seconds_signal = new TimerTask() 
	     {
			public void run() {
				seconds++;
				timerobserver.notifySeconds();
			}
	     };

	     // Creamos un objeto timer que avisará a los tymeinseconds
	     timer = new Timer(); 
	     // Le decimos que una vez cumplido esos segundos active la tarea LimitTask
	     //timer.schedule(reached_limit_time, timelimitinseconds*1000);  
	     
	     //Nueva manera, con esta especificamos que avise a una hora...
	     //Date date = new Date();
	     //date.setTime(date.getTime() + (1000 * timelimitinseconds));
	     timer.schedule(reached_limit_time, when);
	     
	     //Creamos otro objeto para avisar cada segundo de que se debe cambiar el reloj
	     secondstimer = new Timer();
	     
	     //Le decimos que al cumplir 1 segundo avise y que se vuelva a ejecutar cada segundo
	     secondstimer.schedule(seconds_signal, 1000, 1000);
	}
	
	/**
	 * Método cuyo único propósito es parar el reloj
	 */
	public void stop(){
		if(secondstimer != null)
			secondstimer.cancel();
		if(timer != null)
			timer.cancel();
	}
	
	/**
	 * Método que devuelve el valor del tiempo que queda en formato MM:SS en texto
	 * @return Cadena de texto que contiene el tiempo restante para el cronómetro en formato MM:SS
	 */	
	public String getRemainingTimeToString(){
		Date now = new Date();
		now.setTime(now.getTime());
		
		int diff = (int) ((when.getTime()-now.getTime())/1000);
		
		if(diff < 0)
		{
			diff=0;
		}
		
		Time time = new Time();
		
		time.set(diff*1000);
		
		String remaining_time = time.format("%M:%S");
		
		return remaining_time;
	}
	
	/**
	 * Método que devuelve el tiempo que queda en el cronómetro pero en formato entero. 
	 * @return Entero que contiene el tiempo restante en segundos
	 */
	
	public int getRemainingTime(){
		Date now = new Date();
		now.setTime(now.getTime());
		
		int diff = (int) ((when.getTime()-now.getTime())/1000);
		
		if(diff < 0)
		{
			diff=0;
		}
		
		return diff;
	}
	
	/**
	 * Método que devuelve los segundos que llevamos desde que activamos el cronómetro en formato MM:SS
	 * @return String que contiene el tiempo que llevamos desde que activamos el cronómetro en formato MM:SS
	 */
	public String getSecondsToString(){
		String result = new String();
		
		Time time = new Time();
		
		time.set(seconds*1000);
		
		result = time.format("%M:%S");
		
		return result;
	}
	
	/**
	 * Método que devuelve los segundos que llevamos desde que activamos el cronómetro en segundos
	 * @return Entero que contiene el tiempo que llevamos desde que activamos el cronómetro en segundos.
	 */
	public int getSeconds(){
		return seconds;
	}
	
	/**
	 * Método estático que devuelve la diferencia de tiempo en segundos que existe entre una fecha de inicio y una fecha fin.
	 * Se le debe proporcionar un límite de tiempo en segundos, ideal para obtener la diferencia entre dos fechas pero poniendo un tope de tiempo.
	 * @param begin Fecha de inicio que contiene el primer valor de la diferencia que queremos obtener
	 * @param end Fecha de fin que contiene el segundo valor de la diferencia que queremos obtener
	 * @param secondslimit Límite en segundos que se considerará como tope para la diferencia
	 * @return Entero en segundos que muestra la diferencia entre la fecha final y la fecha inicial. Si se ha superado o igualado el límite, se devolverá el límite como máximo.
	 */
	public static int difference(Date begin, Date end, int secondslimit)
	{
		int diff = (int) ((end.getTime()-begin.getTime())/1000);
		
		System.out.println("Diff "+diff + " secondslimit "+secondslimit);
		
		if(diff > secondslimit)
		{
			diff=secondslimit;
		}
		
		return diff;
	}
	
	/**
	 * Método estático que devuelve la diferencia entre dos fechas inicio y fin, pero en modo texto y con el formato MM:SS.
	 * Se le debe proporcionar un límite de tiempo en segundos, ideal para obtener la diferencia entre dos fechas pero poniendo un tope de tiempo.
	 * @param begin Fecha de inicio que contiene el primer valor de la diferencia que queremos obtener
	 * @param end Fecha de fin que contiene el segundo valor de la diferencia que queremos obtener
	 * @param secondslimit Límite en segundos que se considerará como tope para la diferencia
	 * @return Diferencia de tiempo en formato MM:SS que existe entre la fecha fin y la fecha inicial, mostrando como máximo el límite en segundos proporcionado.
	 */
	public static String differenceToString(Date begin, Date end, int secondslimit)
	{
		int x = TimerClass.difference(begin, end, secondslimit);
		
		Time time = new Time();
		
		time.set(x*1000);
		
		String result = time.format("%M:%S");
		
		return result;
		
	}
	
	/**
	 * Método estático que devuelve el tiempo restante desde la fecha actual del sistema y la fecha final proporcionada como parámetro.
	 * @param end Fecha de fin de la que queremos saber cuanto queda para que llegue en segundos
	 * @return Segundos que existen entre la fecha actual del sistema y la fecha proporcionada como parámetro
	 */
	public static int getRemainingTime(Date end)
	{
		Date now = new Date();
		now.setTime(now.getTime());
		
		int diff = (int) ((end.getTime()-now.getTime())/1000);
		
		if(diff < 0)
		{
			diff=0;
		}
		
		return diff;
	}
	
}