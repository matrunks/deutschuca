package com.manuel.pfc;

import java.util.ArrayList;
import java.util.Collection;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.DefaultMessageEventRequestListener;
import org.jivesoftware.smackx.MessageEventManager;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;

import android.os.StrictMode;

/**
 * Clase que sirve de soporte para las conexiones básicas con el servidor bajo el protocolo XMPP
 * Proporciona métodos de conexión, desconexión, envío de mensajes, inicio de sesión...
 * @author Manuel Jesús Pérez Zurera 
 *
 */ 
public class ChatHelper implements MessageListener{
 
    public XMPPConnection connection;
    private ArrayList<String> users;
    private FileTransferManager manager;
    private MessageEventManager messageEventManager;
    private String username;
    private String password;
    private ArrayList<Message> messages_queue;
    
    /**
     * Método que nos gestionará una conexión al servidor descrito por el puerto descrito
     * @param servername Nombre del dominio o IP del servidor al que queremos conectar
     * @param port Puerto en formato entero por el que intentaremos conectarnos al servidor
     * @throws XMPPException
     * @return Devuelve true si la conexión ha sido satisfactoria y false en caso contrario     
     */
    public boolean connect(String servername, int port) throws XMPPException{
    	if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
    	     // only for honeycomb and newer versions
    		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        	StrictMode.setThreadPolicy(policy); 
    	}
    	
    	//ConnectionConfiguration config = new ConnectionConfiguration("proyectosii.uca.es",5222);
    	//ConnectionConfiguration config = new ConnectionConfiguration("vps2367-cloud.comalis.net",5222);
    	ConnectionConfiguration config = new ConnectionConfiguration(servername,port);
    	config.setSASLAuthenticationEnabled(true); 
    	config.setReconnectionAllowed(true);
    	SASLAuthentication.supportSASLMechanism("DIGEST-MD5", 0);
    	connection = new XMPPConnection(config);
   
    	messages_queue = new ArrayList<Message>();

    	try {
    		//while(!connection.isConnected()){
    			System.out.println("Connecting");
    			try{
    				connection.connect();
    			}
    			catch (Exception e)
    			{
    				System.out.println("Cannot connect to the server");
    				e.printStackTrace();
    				return false;
    			}
    			
    			if(connection.isConnected()){
                	System.out.println("Connection connected");
                }
    		//}
    		
    		//Con esto autocontestamos a los recibo,s pero tambien a los mios ya que estamos en una MUC ROOM y recibimos los nuestros también
    		//DeliveryReceiptManager.getInstanceFor(connection).setAutoReceiptsEnabled(true);
    		
    		messageEventManager = new MessageEventManager(connection);
    		
    		//Escuchadores de mensajes para responder
    		messageEventManager.addMessageEventRequestListener(new DefaultMessageEventRequestListener() {
    	          public void deliveredNotificationRequested(
    	              String from,
    	              String packetID,
    	              MessageEventManager messageEventManager) {
    	              super.deliveredNotificationRequested(from, packetID, messageEventManager);
    	              // DefaultMessageEventRequestListener automatically responds that the message was delivered when receives this request
    	              System.out.println("Delivered Notification Requested (" + from + ", " + packetID + ")");
    	          }
    	          
    	          /*public void displayedNotificationRequested(
	                  String from,
	                  String packetID,
	                  MessageEventManager messageEventManager) {
	                  super.displayedNotificationRequested(from, packetID, messageEventManager);
	                  // Send to the message's sender that the message was displayed
	                  messageEventManager.sendDisplayedNotification(from, packetID);
	              }

	              public void composingNotificationRequested(
	                  String from,
	                  String packetID,
	                  MessageEventManager messageEventManager) {
	                  super.composingNotificationRequested(from, packetID, messageEventManager);
	                  // Send to the message's sender that the message's receiver is composing a reply
	                  messageEventManager.sendComposingNotification(from, packetID);
	              }*/

    	          /*public void offlineNotificationRequested(
    	              String from,
    	              String packetID,
    	              MessageEventManager messageEventManager) {
    	              super.offlineNotificationRequested(from, packetID, messageEventManager);
    	              // The XMPP server should take care of this request. Do nothing.
    	              System.out.println("Offline Notification Requested (" + from + ", " + packetID + ")");
    	          }*/
    	      });
    		
    		
    	} catch (Exception e) {
	    	System.out.println("Cannot connect");
	    	connection.disconnect();
	        e.printStackTrace();
	        return false;
	    }
    	
		return true; 	
    }
    
    /**
     * Método que nos gestionará el inicio de sesión con una acreditación dada
     * @param username Nombre de usuario con el que queremos conectarnos
     * @param password Clave mediante la cual se validará el inicio de sesión
     * @param servername Nombre del dominio o IP del servidor al que queremos conectar
     * @param port Puerto en formato entero por el que intentaremos conectarnos al servidor
     * @return Devuelve true si el inicio de sesión ha sido satisfactorio y false en caso contrario    
     */
    public boolean login(String username, String password, String servername, int port)
    {
		this.username=username;
		this.password=password;
		
		boolean login = true;
		
		if(!connection.isConnected())
		{
			try {
				connect(servername,port);
			} catch (XMPPException e) {
				e.printStackTrace();
			}
		}
		
		try {
			connection.login(username, password);
		} catch (XMPPException e) {
			System.out.println("Cannot login");
	        e.printStackTrace();
	        connection.disconnect();
	        login = false;
		}
		
		if(login == true)
		{
			if(connection.isAuthenticated()){
	        	System.out.println("Connection authenticated");
	        }
	        
	        String status = "available";

	        Presence presence = new Presence(Presence.Type.available, status, 24,
	                Presence.Mode.available);
	        presence.setStatus(status);
	        connection.sendPacket(presence);
	        
	        if(connection.isSendPresence()){
	        	System.out.println("Connection sent presence");
	        } 
	        
	        return true;
		}
		else
		{
			return false;
		}
        //enableFileTransfer();
    }
    
    /**
     * Método observador que nos devuelve el nombre del usuario
     * @return Devuelve el nombre de usuario en un objeto String   
     */
    public String getUsername(){
    	return username;
    }
    
    /**
     * Método observador que nos devuelve la contraseña del usuario
     * @return Devuelve la clave de usuario en un objeto String   
     */
    public String getPassword(){
    	return password;
    }
    
    public MessageEventManager getMessageEventManager(){
    	return messageEventManager;
    }
    
    public void enableFileTransfer(){
    	manager = new FileTransferManager(connection);
    }
    
    public FileTransferManager getFileTransferManager(){
    	return manager;
    }
    
    /*public void sendFileTo(java.io.File file,String userID){
    	// Create the outgoing file transfer
    	if(file==null){
    		System.out.println("File is null");
    	}
    	
    	System.out.println("Sending file to: "+userID);
    	
        OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(userID);
        
        // Send the file
        try {
			transfer.sendFile(file, "You won't believe this!");
		} catch (XMPPException e) {
			System.out.println("Foto no enviada");
			e.printStackTrace();
		}
        
        /*while (!transfer.isDone())
        {

            if (transfer.getStatus() == FileTransfer.Status.refused)
            {
                System.out.println("Could not send the file to " + userID + ".");
                return;
            }

            if (transfer.getStatus() == FileTransfer.Status.error)
            {

                System.out.println("Cannot send the file because an error occured during the process.");
                return;
            }

        }
        
        System.out.println(transfer.getFileName() + "has been successfully transferred.");

        //System.out.println("The Transfer is " + transfer.isDone());
          
    }*/
    
    /**
     * Método que devuelve la cola de mensajes por enviar
     * @return Devuelve un objeto ArrayList con la cola de mensajes que queda por enviar
     */
    public ArrayList<Message> getMessagesQueue(){
    	return messages_queue;
    }
 
    /**
     * Método que envía un mensaje dado a una cuenta dada
     * @param message Mensaje en formato String que queremos enviar
     * @param to Destinatario en formato String de la persona a la que queremos enviar el mensaje  
     * @throws XMPPException 
     */
    public void sendMessage(String message, String to) throws XMPPException
    {
    	Chat chat = connection.getChatManager().createChat(to, this);
    	chat.sendMessage(message);
    }
    
    /**
     * Método que envía un mensaje dado a una sala
     * @param text Mensaje en formato String que queremos enviar
     * @param room Sala de destino en formato String a la que queremos enviar el mensaje  
     * @throws XMPPException 
     * @return Devuelve true si se ha podido enviar el mensaje, false en caso contrario.
     */
    public boolean sendGroupMessage(String text, String room) throws XMPPException{
    	 Message message = new Message(room, Message.Type.groupchat);
         message.setBody(text);

         //Comprobamos si estamos conectados
         if(connection.isConnected()){
        	        	 
        	 MessageEventManager.addNotificationsRequests(message, false, true, false, false);
        	 
        	 //Metemos en la cola de mensajes el mensaje enviado
        	 messages_queue.add(message);
        	 
        	 connection.sendPacket(message);
        	 return true;
         }
         else{
        	 System.out.println("No hay conexión, no se puede enviar el mensaje");
        	 return false;
         }
         
    }
 
    /*public ArrayList<String> displayBuddyList()
    {		
    	users = new ArrayList<String>();
    	int i=0;
    	try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	Roster roster = connection.getRoster();
    	Collection<RosterEntry> entries = roster.getEntries();
    	ProviderManager.getInstance().addIQProvider("vCard", "vcard-temp",
                new VCardProvider());
        VCard card = null;
 
    	if(entries.isEmpty()){
    		System.out.println("Roster is empty");
    		return users=null;
    	}
    	
    	System.out.println("Displaying buddy list");
    	
    	for(RosterEntry r:entries)
    	{
    		users.add(r.getUser());
    		Presence presence = roster.getPresence(r.getUser());
    		
    		String jid = presence.getFrom();
    		//String name = new String();
            //name = card.getField("FN");
            String status = presence.getType().name();
    		
		    
		    System.out.println(jid + " " + " " + presence); 
		    i++;
		    
		    /*byte[] imgs = card.getAvatar();
            if (imgs != null) {
                int len = imgs.length;
                Bitmap img = BitmapFactory.decodeByteArray(imgs, 0, len);
            }
    	}
    	return users;
    }*/
    
    
    /**
     * Método que devuelve una lista con los usuarios conectados 
     * @return Devuelve una lista con los usuarios conectados de la lista de amigos
     */
    public ArrayList<String> displayConnectedList(){
    	users = new ArrayList<String>();
    	Roster roster = connection.getRoster();
    	Collection<RosterEntry> entries = roster.getEntries();
    	
    	if(entries.size()==0){
    		return users=null;
    	}
    	
    	for(RosterEntry r:entries)
    	{
    		Presence presence = roster.getPresence(r.getUser());
    		//System.out.println("Displaying buddy list "+ r.getUser() + roster.getPresence(r.getUser()));
    		if (!presence.toString().contains("unavailable") ) { //Si está conectado
    			users.add(r.getUser().toString());
        	}
    	}
    	return users;
    }
    
    /*public void addBuddy(String buddy){
    	Presence subscribe = new Presence(Presence.Type.subscribe);
    	subscribe.setTo(buddy);
    	connection.sendPacket(subscribe);
    	
    	Presence subscribed = new Presence(Presence.Type.subscribed);
    	subscribed.setTo(buddy);
    	connection.sendPacket(subscribed);
    }*/
 
    /**
     * Método que nos desconecta del servidor
     */
    public void disconnect()
    {
    	System.out.println("Disconnecting");
    	connection.disconnect();
    	if(!connection.isConnected()){
    		System.out.println("Disconnected");
    	}
    }
    
    /**
     * Método que nos devuelve el objeto XMPPConnection que contiene la información de conexión
     * @return Devuelve el objeto XMPPConection que alberga la conexión con el servidor
     */
    public XMPPConnection getConnection(){
    	return connection;
    }
 
    /**
     * Método heredado que nos permite procesar un mensaje
     */
    public void processMessage(Chat chat, Message message)
    {
    	if(message.getType() == Message.Type.chat)
    		System.out.println(chat.getParticipant() + " says: " + message.getBody());
    }
    
}