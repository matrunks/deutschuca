package com.manuel.pfc;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Clase que extiende de Fragment y que gestiona el fragmento de las actividades a realizar por el comisario.
 * @author Manuel Jesús Pérez Zurera 
 *
 */

public class AssignmentFragment extends Fragment {
	private TextView text;
	private View view;
	private onAssignmentFragmentListener mCallback;
	private BlocData bloc;
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			   Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.assignment_fragment,
				   container, false);
		setRetainInstance(true);
		
		text = (TextView) view.findViewById(R.id.assignment_text);
		
		mCallback.onAssignmentFragmentFinish(true);
		return view;
	}
	
	/**
	 * Método necesario para fijar el texto de la actividad en la vista del Assignment
	 * @param assignment_question El texto en formato String que queremos fijar a la vista 
	 *
	 */
	public void setText(String assignment_question){
		text.setText(assignment_question);
	}
	
	
	/**
	 * Método necesario para fijar el blog de notas escrito por el comisario
	 * @param bloc Código en formato String 
	 *
	 */
	public void setBloc(BlocData bloc){
		this.bloc = bloc;
		putNotesOnBoxes();
	}
	
	/**
	 * Método que se encarga de recoger las notas almacenadas y colocarlas en sus campos correspondientes de la vista.
	 */
	private void putNotesOnBoxes(){
		int id = getResources().getIdentifier("maxHints", "integer", "com.manuel.pfc");
		int max = getResources().getInteger(id);
		for(int i=0; i<max; i++)
		{
			String note = null;
			
			id = getResources().getIdentifier("hintText"+(i+1), "id", "com.manuel.pfc");
			
			EditText text = (EditText) view.findViewById(id);
			
			if(this.bloc.existNote(i))
			{
				note = this.bloc.getNote(i);
				
			    text.setText(note);
				
				System.out.println("poniendo "+ note);
				
			}
			
		}
	}
	
	/**
	 * Este método se encarga de almacenar las notas en su correcto orden para poder volver a mostrarlas más tarde
	 *
	 */
	public void saveNotes(){
		//Aqui debemos guardar todas las pistas recibidas en el bloc
		int id = getResources().getIdentifier("maxHints", "integer", "com.manuel.pfc");
		int max = getResources().getInteger(id);
		for(int i=0; i<max; i++)
		{
			String note = null;
			
			id = getResources().getIdentifier("hintText"+(i+1), "id", "com.manuel.pfc");
			
			EditText text = (EditText) view.findViewById(id);
			
			note = text.getText().toString();
			
			System.out.println("guardando "+ note);
			
			this.bloc.saveNote(note, i);
		}
		
	}
	
	// Container Activity must implement this interface
    public interface onAssignmentFragmentListener {
        public void onAssignmentFragmentFinish(boolean finish);
    }
    
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onAssignmentFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
