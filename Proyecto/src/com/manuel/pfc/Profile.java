package com.manuel.pfc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Clase que extiende Activity y gestiona la vista del perfil
 * Permite introducir un nombre, y borrar la partida anterior por si queremos comenzar de nuevo
 * @author matrunks
 *
 */

public class Profile extends Activity{
	
	/**
	 * Constructor que inicializa la activity
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		//archivo xml
		setContentView(R.layout.profile_layout);
	}
	
	/**
	 * Método que se ejecuta automáticamente al pulsar el botón Add.
	 * Recoge el nombre introducido por teclado y invoca a la función de insertar usuario en la BD
	 * @param button Vista del botón pulsado
	 */
	public void onAddClick(View button){
		TextView userName = (TextView) findViewById (R.id.nametext);
		
		Intent intent = new Intent();
		//CUIDADO ME INTERESA CONSERVAR EL NOMBRE AL VOLVER A MAIN ACTIVITY
		intent.setClass(this, MainActivity.class);
		
		//necesitamos usar el .getText() por que bookTitle es un autocompletetextview y uno de sus atributos es la cadena
		intent.putExtra("UserName", userName.getText().toString());
		insertUser(userName.getText().toString());
	
		startActivity(intent);
	}
	
	/**
	 * Método que se encarga de insertar el nombre de un usuario.
	 * Sólo se puede tener un nombre de usuario
	 * @param userName El nombre de usuario en formato String
	 */
	private void insertUser(String userName){
		DatabaseHelper databaseHelper = new DatabaseHelper (this);
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		
		//un contentvalues es como un hashmap
		ContentValues cv = new ContentValues();
		
		//nombre,valor
		cv.put(DatabaseHelper.USERNAME, userName);
		
		//Si existe la tabla usuarios la borro
		db.execSQL( "DROP TABLE IF EXISTS users" );
		
		//Creamos la tabla usuarios, con un id entero autoincremental que será la primary key y un nombre
		db.execSQL( "CREATE TABLE users (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT);");
		
		//Insertamos en la tabla usuarios, el valor del campo cv dentro de la columna username
		db.insert("users", DatabaseHelper.USERNAME, cv);
		
		//Ciero la conexión con la BD
		db.close();		
	}
	
	/**
	 * Método que llamará a la función de borrado de partidas en la base de datos, anza pregunta de confirmación antes de proceder
	 * @param button Vista del botón que se acaba de pulsar.
	 */
	public void onDeleteClick(View button){
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		            //Yes button clicked
		        	deleteSaveGame();
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            //No button clicked
		            break;
		        }
		    }
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Se borrarán las partidas guardadas. ¿Está seguro?").setPositiveButton("Si", dialogClickListener)
		    .setNegativeButton("No", dialogClickListener).show();
		
	}
	
	/**
	 * Método que borra las partidas de nuestro perfil y nuestro nombre.
	 */
	private void deleteSaveGame()
	{
		//Borramos las partidas guardadas y el nombre
		DatabaseHelper databaseHelper = new DatabaseHelper (this);
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		
		//Si existe la tabla usuarios y puntuaciones las borro
		db.execSQL( "DROP TABLE IF EXISTS users" );
		db.execSQL( "DROP TABLE IF EXISTS marks" );
		
		
		//Creamos la tabla usuarios, con un id entero autoincremental que será la primary key y un nombre
		db.execSQL( "CREATE TABLE users (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT);");
		//Crea una tabla llamada puntuaciones con un id, un numero de actividad, un entero de aciertos, un entero de fallos y una fecha que por defecto es la actual con horas y minutos
		db.execSQL( "CREATE TABLE marks (_id INTEGER PRIMARY KEY AUTOINCREMENT, activity INTEGER, corrects INTEGER, fails INTEGER, date DATETIME DEFAULT CURRENT_TIMESTAMP, time TEXT);");
	
		db.close();
		
		Intent intent = new Intent();
		//CUIDADO ME INTERESA CONSERVAR EL NOMBRE AL VOLVER A MAIN ACTIVITY
		intent.setClass(this, MainActivity.class);
		
		showToast("Perfil borrado");
		
		//Volvemos al menú
		startActivity(intent);
	}
	
	/**
	 * Método para mostrar por pantalla una notificación de android 
	 * @param toast El texto que queremos mostrar por notificación en pantalla
	 */
	private void showToast(final String toast)
	{
	    runOnUiThread(new Runnable() {
	        public void run()
	        {
	            Toast.makeText(Profile.this, toast, Toast.LENGTH_SHORT).show();
	        }
	    });
	}
	

}
