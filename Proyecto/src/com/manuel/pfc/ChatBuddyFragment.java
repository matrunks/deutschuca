package com.manuel.pfc;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/**
 * Clase que hereda de Fragment y que gestiona el fragmento de la pantalla de chat
 * @author Manuel Jesús Pérez Zurera 
 */

public class ChatBuddyFragment extends Fragment {
	private LinearLayout ll;
	private View view;
	private onChatBuddyListener mCallback;
	//private onChatBuddyListener mCallback;
	
	@Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {
	    view = inflater.inflate(R.layout.chat_buddy_fragment,
	        container, false);
	    ll = (LinearLayout) view.findViewById(R.id.linearLayout);
	    
	    setRetainInstance(true);
	    
	    //Damos el mensaje de que hemos terminado
		mCallback.onChatBuddyFinish(true);
		
	    return view;
	  }
	
	/**
	 * Método que activa la visibilidad del botón de chat
	 *
	 */
	public void activateButton(){
		Button b = (Button) view.findViewById(R.id.btn_multi_chat);
		b.setVisibility(View.VISIBLE);
	}
	
	 // Container Activity must implement this interface
    public interface onChatBuddyListener {
        public void onChatBuddyFinish(boolean finish);
    }
    
    /**
	 * Método que cambia el nombre de empezar partida por seguir partida
	 *
	 */
    public void setButtonName(String name)
    {
    	Button b = (Button) view.findViewById(R.id.btn_multi_chat);
		b.setText(name);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onChatBuddyListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
	
    /**
	 * Método para añadir más botones a la vista
	 *
	 */
	public void post(final Button btn_user, final LayoutParams params){
		ll.post(new Runnable() {
			public void run(){
				ll.addView(btn_user,params);
			}
		});
	}
	
	/**
	 * Método para eliminar botones de la vista
	 *
	 */
	public void post(final Button btn_user){
		ll.post(new Runnable() {
			public void run(){ //Debemos de ejecutarlo en un hilo para que se refresquen los cambios en tiempo real
				ll.removeView(btn_user);
			}
        });
	}
	
}
