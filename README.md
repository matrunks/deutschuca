# README #

VocabTrainer A1 is an Android APP to empower foreign language learning. It is available under a free GPL license. It work in Android 2.3 or higher smartphones.

The APP has two parts:
* In the first part, the APP offers nine test-like activities to learn vocabulary.
* In the second one, the APP builds a gymakana using text-chat and QR-code scanning.

